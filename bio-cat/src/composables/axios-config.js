import axios from 'axios';

const common_suffix = "/biodiversitat/";
const api = axios.create({
  baseURL: window.location.href.substr(0, window.location.href.indexOf(common_suffix) ) + common_suffix, // Flask API base URL 
});

export default api;
