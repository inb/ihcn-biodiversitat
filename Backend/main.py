# --------------------------------------------------------------------------------------------------------------------
# --- IMPORTS --- #
# --------------------------------------------------------------------------------------------------------------------
import os
import re
import json
import configparser
import utils as uti

from openpyxl import load_workbook

# --------------------------------------------------------------------------------------------------------------------
# --- CONSTANTS: PATHS TO CHECK --- #
# --------------------------------------------------------------------------------------------------------------------
# Get the path to the directory containing the script
script_directory = os.path.dirname(os.path.abspath(__file__))
os.chdir(script_directory)

# Get the path to the config.ini file
config_file_path = os.path.join(script_directory, "Configuration", "config.ini")
config = configparser.ConfigParser()
config.read(config_file_path)

WORKED_DIR = config.get("ROUTES", "worked_dir")
EXCELS_DIRPATH = config.get("ROUTES", "excels_dirpath")
TMP_DIRPATH = config.get("ROUTES", "tmp_dirpath")
JSON_DIRPATH = config.get("ROUTES", "json_dirpath")
REGISTER_DIRPATH = config.get("ROUTES", "register_dirpath")

# --------------------------------------------------------------------------------------------------------------------
# --- TRANSFORMATION TO JSON,  WITH DISCOVERERS AND SPECIES SPLITTED--- #
# --------------------------------------------------------------------------------------------------------------------


def file_parser() -> str:
    """
    Writes json from excel file and adds TaxId and modify the specie column.
    Creates a json register file
    Return: string with the result if done
    """

    # Get the list of all the files in the directory
    all_excels: list[str] = uti.files_extractor(EXCELS_DIRPATH, "excel")
    count_files: int = len(all_excels)

    # For each file in the directory of excels_file_path
    for excel in all_excels:

        # Open workbook
        wb = load_workbook(filename=EXCELS_DIRPATH + excel)
        sheet = wb.active

        # Variable to store all the data in excel
        excel_data: list[list[str]] = []

        # Store the filename of the excel file
        if ".xlsx" in excel:
            file_name = excel.split(".xlsx")[0]
        elif ".XLSX" in excel:
            file_name = excel.split(".XLSX")[0]

        # To track the runing of the script
        print(file_name)

        # Get indexes of header row, specie column
        i: int = 0
        row_index: int = 0
        especie_index: int = 0

        # Store all dat from specie column from the excel file
        especie_column_list: list[str] = []
        origen_column_list: list[str] = []

        # Iterate over each row in the excel file
        for value in sheet.iter_rows(values_only=True):
            i += 1
            row: list[str] = list(value)

            # Get the index of the column specie and row header
            if "Espècie" in row:
                row_index: int = i
                for value in row:
                    if "Espècie" in value:
                        especie_index: int = row.index(value)
                        break
            elif "Espècie amb autoria" in row:
                row_index: int = i
                for value in row:
                    if "Espècie amb autoria" in value:
                        especie_index: int = row.index(value)
                        break
            elif "Especie" in row:
                row_index: int = i
                for value in row:
                    if "Especie" in value:
                        especie_index: int = row.index(value)
                        break

            # Fill the especie_column_list with the species
            especie_column_list.append(row[especie_index])
            # Fill excel_data variable with each row in excel
            # Avoid blank extra columns and rows
            clean_row: list[str] = []
            for value in row:
                if value != None and type(value) == str:
                    clean_row.append(value.strip())
                else:
                    clean_row.append(value)

            excel_data.append(clean_row)

        # Column specie without headers
        only_specie_column: list[str] = especie_column_list[row_index:]

        # Get the colum specie without none(mainly the footer)
        specie_colum_dirty: list[str] = list(
            filter(lambda x: x is not None, only_specie_column)
        )

        # Get cleaned string from specie column.
        specie_column: list[str] = []

        # Some excels have additional data in the specie column suach as origin species
        # Up now is removed from the specie column

        for i in specie_colum_dirty:
            if "]" in i:
                specie_column.append(
                    re.sub(r"(\[.*\])", "", i).strip().replace("\xa0", " ")
                )
            elif "[" in i:
                specie_column.append(
                    re.sub(r"(\[.*)", "", i).strip().replace("\xa0", " ")
                )
            else:
                specie_column.append(i.strip().replace("\xa0", " "))

        # Clean the unwanted blank items
        specie_column = [item for item in specie_column if item.strip()]

        # Only species in the list from html
        species_dict: dict[dict] = uti.get_only_specie(TMP_DIRPATH, file_name + ".html")

        # Check if all the species ate in italic format
        if type(species_dict) == str:
            raise Exception("Not all the species where in italic check the docs.")
        else:
            # Get cleaned string from specie dict.
            species_dict_cleaned: list[str] = []

            for i in species_dict[file_name]:
                species_dict_cleaned.append(i.strip())

            # Discoverers
            authors: list[str] = uti.separate_autor(specie_column, species_dict_cleaned)

            # For plantes
            # authors: list[str] = uti.get_difference(specie_column, species_dict_cleaned)

            # Get the data
            only_data_without_headers: list[list[str]] = excel_data[row_index:]

            # principal header without none extra columns
            main_header: list[str] = list(
                filter(lambda x: x is not None, excel_data[2])
            )
            title_header: list[str] = list(
                filter(lambda x: x is not None, excel_data[0])
            )

            # Replace the \n with a space from the main header.
            main_header_clean: list[str] = []

            for i in main_header:
                main_header_clean.append(i.replace("\n", " ").strip())

            # Search the colum origin if found to count all endemic species.
            endemics: int = 0

            if "Origen i endemisme" in main_header_clean:
                endemics = main_header_clean.index("Origen i endemisme")
            elif "Endemisme" in main_header_clean:
                endemics = main_header_clean.index("Endemisme")
            elif "Origen" in main_header_clean:
                endemics = main_header_clean.index("Origen")
            elif "Estatus Catalunya" in main_header_clean:
                endemics = main_header_clean.index("Estatus Catalunya")
            else:
                endemics = "No origen or endemisme column"

            # If found get the values
            if endemics != "No origen or endemisme column":
                for value in sheet.iter_rows(values_only=True):
                    row: list[str] = list(value)
                    origen_column_list.append(row[endemics])

                # Column origin without headers
                only_origen_column: list[str] = origen_column_list[row_index:]

                # Get the colum origin without none(mainly the footer)
                origen_colum_dirty: list[str] = list(
                    filter(lambda x: x is not None, only_origen_column)
                )

                # Get cleaned string from origin column.
                origen_column: list[str] = []

                for i in origen_colum_dirty:
                    origen_column.append(i.strip().replace("\xa0", " "))

                # Search if the are endemic species in the column to add it in the registry
                count_en: int = 0

                for value in origen_column:
                    if value.startswith("Endèmica") and "mediterrània" not in value:
                        count_en += 1
                    elif value.startswith("Endemisme"):
                        count_en += 1
                    elif value.startswith("Subendemisme"):
                        count_en += 1
                    elif (
                        "Barcelona" in value
                        or "Montserrat" in value
                        or "Lleida" in value
                    ):
                        count_en += 1
                    elif "Catalunya" in value and "Introduïda" not in value:
                        count_en += 1
                    elif value.startswith("Pirineus") or "Pirinenc" in value:
                        count_en += 1
                    elif "SÍ" in value:
                        count_en += 1
                    # Found a typo in one excel file
                    elif value.startswith("Edemisme"):
                        count_en += 1

            # Get only the data of every row with the length of the header, clean the extra columns parsed
            only_data_without_headers_real: list[list[str]] = []

            for row in only_data_without_headers:
                real_row: list[str] = row[: len(main_header)]
                only_data_without_headers_real.append(real_row)

            # Adding the column of author and taxid and filr_name as none for the moment
            for i in range(len(only_data_without_headers_real)):
                only_data_without_headers_real[i].append(None)
                only_data_without_headers_real[i].append(None)

            # Take the footers out
            only_data_without_headers_footers: list[list[str]] = (
                only_data_without_headers_real[: len(specie_column)]
            )

            # Get legend data to json to estudis moleculars
            # ----------------------------------------------------------

            raw_footers: list[list[str]] = only_data_without_headers_real[
                len(authors) :
            ]

            # Clean footers [list]
            cleaned_footers: list[list[str]] = [
                uti.remove_nones(sublist) for sublist in raw_footers
            ]
            cleaned_footers: list[list[str]] = [
                uti.remove_nones(sublist)
                for sublist in cleaned_footers
                if len(uti.remove_nones(sublist)) > 0
            ]
            cleaned_footers: list[str] = [elem[0] for elem in cleaned_footers]

            # Store all the footers in a file
            # If the file exists, read it; if not, create an empty list

            if os.path.exists(REGISTER_DIRPATH + "Footers.json"):
                with open(REGISTER_DIRPATH + "Footers.json", "r") as file:
                    data_f = json.load(file)

            else:
                data_f: list[dict] = []

            # Add the data
            if cleaned_footers == []:
                updated_data_f: dict = {
                    "file": file_name,
                    "footers": "No footer in this file",
                }
            else:
                updated_data_f: dict = {"file": file_name, "footers": cleaned_footers}

            # Update existing data with the keys from updated data
            match_dict_f: dict = next(
                (d for d in data_f if d.get("file") == updated_data_f.get("file")), None
            )
            if match_dict_f:
                match_dict_f.update(updated_data_f)
            else:
                data_f.append(updated_data_f)

            # Sort the JSON data items by the first string key
            sorted_data_f: list[dict] = sorted(
                data_f, key=lambda x: x.get("file", "").lower()
            )

            # Write updated data to JSON file
            with open(REGISTER_DIRPATH + "Footers.json", "w", encoding="utf8") as f:
                json.dump(sorted_data_f, f, indent=4, ensure_ascii=False)

            #! Iter over data and if it found refs like "(1)" replece it withe the  corresponding data in the footer
            refs_list: list[str] = [
                "(1)",
                "(2)",
                "(3)",
                "(4)",
                "(5)",
                "(6)",
                "(7)",
                "(8)",
                "(9)",
                "(10)",
                "(11)",
                "(12)",
                "(13)",
                "(14)",
                "(15)",
                "(16)",
                "(17)",
                "(18)",
                "(19)",
                "(20)",
                "[1]",
                "[2]",
                "[3]",
                "[4]",
                "[5]",
                "[6]",
                "[7]",
                "[8]",
                "[9]",
                "[10]",
                "[11]",
                "[12]",
                "(21)",
                "(22)",
                "(23)",
                "(24)",
                "(25)",
                "(26)",
                "(27)",
                "(28)",
                "(29)",
                "(30)",
                "(31)",
                "(32)",
                "(33)",
                "(34)",
                "(35)",
                "(36)",
                "(37)",
            ]

            # uicn_index =  0
            # uicn_count = uicn_count + 1

            # Index columna estudis moleculars
            estudis_index: int = 0

            for index, item in enumerate(main_header_clean):

                if "Estudis" in item:
                    estudis_index: int = index
                    break
                elif "Observacions" in item:
                    estudis_index: int = index
                    break

            # Change the annotation in the column os estudis
            if estudis_index != 0:
                for i, sublista in enumerate(only_data_without_headers_footers):
                    if sublista[estudis_index] is not None:
                        estudis_val = str(sublista[estudis_index])
                        refs_found = [ref for ref in refs_list if ref in estudis_val]
                        if len(refs_found) > 0:
                            footer_data_list = uti.get_ref_data(
                                refs_found, cleaned_footers
                            )
                            if footer_data_list:
                                updated_elem = estudis_val
                                for ref, footer_data in zip(
                                    refs_found, footer_data_list
                                ):
                                    updated_elem = updated_elem.replace(
                                        ref, (ref + " " + footer_data)
                                    )
                                try:
                                    estudis_val = int(estudis_val)
                                except:
                                    pass
                                sublista[sublista.index(estudis_val)] = updated_elem

                #!---------------------------------------------------------------
                # For quironomids #
                for index, item in enumerate(main_header_clean):
                    if "Estadi" in item:
                        estadi_list = []  # Initalise estadi list values
                        estadi_index = index  # Get estadi column index
                        for i, sublista in enumerate(only_data_without_headers_footers):
                            estadi_val = sublista[estadi_index]
                            if sublista[estadi_index] == None:
                                estadi_val = ""
                            estadi_list.append(
                                estadi_val
                            )  # Append estadis values to list
                        # Convert to dict the footer
                        footer_dict: dict = uti.convert_array_to_dict(cleaned_footers)
                        # Get the new estadi list with footers replaced
                        new_estadi_list: list = [
                            item if item != "" else None
                            for item in uti.replace_elements(estadi_list, footer_dict)
                        ]
                        # Replace the new values to data
                        for i, sublista in enumerate(only_data_without_headers_footers):
                            sublista[estadi_index] = new_estadi_list[i]

            #!---------------------------------------------------------------
            # Fill authors column
            # Make sure both lists are the same length!! (Specie column - author/dicoverer column)
            # ----- For debbuggin only start-------
            # print(len(specie_column))
            # print(len(authors))
            # print()

            # # Iterate over both lists up to the length of the shorter list
            # min_length = min(len(specie_column), len(authors))

            # for i in range(min_length):
            #     print((specie_column[i], authors[i]))

            # # Identify the list with extra elements and print those elements
            # if len(specie_column) > len(authors):
            #     print("\nExtra species without corresponding authors:")
            #     for specie in specie_column[min_length:]:
            #         print(specie)
            # elif len(authors) > len(specie_column):
            #     print("\nExtra authors without corresponding species:")
            #     for author in authors[min_length:]:
            #         print(author)
            # ----- For debbuggin only end-------

            for i in range(len(only_data_without_headers_footers)):
                if specie_column[i] == species_dict_cleaned[i]:
                    # if there is no author in the column
                    only_data_without_headers_footers[i][-2] = None
                else:
                    if len(authors) == 0:
                        only_data_without_headers_footers[i][-2] = None
                    elif authors[i] == "":
                        only_data_without_headers_footers[i][-2] = None
                    elif "(" in authors[i] or ")" in authors[i]:
                        only_data_without_headers_footers[i][-2] = (
                            authors[i].replace("(", "").replace(")", "")
                        )
                    else:
                        only_data_without_headers_footers[i][-2] = authors[i]

            # Change the column specie with only the specie
            for i in range(len(only_data_without_headers_footers)):
                only_data_without_headers_footers[i][especie_index] = (
                    species_dict_cleaned[i]
                )

            # Fill the last column with the name of the file where the species come from to use it in the db
            for i in range(len(only_data_without_headers_footers)):
                only_data_without_headers_footers[i][-1] = file_name

            # Edit the headers to adapt the added columns
            # Add 2 columns for the author and the file title
            main_header_clean.append(None)
            main_header_clean.append(None)

            # Rename the columns that had been added
            main_header_clean[especie_index] = "Espècie"
            main_header_clean[-2] = "Autor"
            main_header_clean[-1] = "Pertany_A"

            # Final result of data construction
            all_data: list[list[str]] = []
            # Append header cleaned
            all_data.append(title_header)
            all_data.append(main_header_clean)
            # Append species data
            for specie in only_data_without_headers_footers:
                all_data.append(specie)

            # To check the real length of the authors column
            authors_column: list[str] = []

            # Get the index of the column
            if "Autor" in all_data[1]:
                dicoverer_index: int = all_data[1].index("Autor")

            # Append all the info in a list with the author column index
            for item in all_data[2:]:
                authors_column.append(item[dicoverer_index])

            # Slice string to remove last 5 characters from string(.xlsx)
            size_name_file: int = len(excel)
            excel: str = excel[: size_name_file - 5]

            # Convert to json fromat the data to the database structure
            # ----------------------------------------------------------

            # Columns of the file to  use as the header
            columns: list[str] = main_header_clean

            # If the file exists, read it; if not, create an empty list
            if os.path.exists(JSON_DIRPATH + file_name + ".json"):
                with open(JSON_DIRPATH + file_name + ".json", "r") as file:
                    data = json.load(file)
            else:
                data: list[dict] = []

            # Convert the worksheet data to a list of dictionaries
            updated_data: list[dict] = []
            for i in only_data_without_headers_footers:
                updated_data.append(dict(zip(columns, i)))

            # Find the index of the first dictionary in the data that matches the updated data
            # Update existing data with the keys from updated data
            for updated_dict in updated_data:
                match_dict: dict = next(
                    (
                        d
                        for d in data
                        if d.get("Espècie") == updated_dict.get("Espècie")
                    ),
                    None,
                )
                if match_dict:
                    match_dict.update(updated_dict)
                else:
                    data.append(updated_dict)
            # Write updated data to JSON file
            with open(JSON_DIRPATH + file_name + ".json", "w", encoding="utf8") as f:
                json.dump(data, f, indent=4, ensure_ascii=False)

            # --- SET REGISTER FILE --- #
            # ------------------------------------------------------------------------------
            # If there is no discoverers set the count a 0

            has_non_none_string = False

            for elem in authors_column:
                if isinstance(elem, str) and elem is not None:
                    has_non_none_string: bool = True
                    break

            if has_non_none_string:
                authors_log: int = len(set(authors_column))
            else:
                authors_log: int = 0

            # Count the species in the file
            species: int = len(species_dict_cleaned)

            # Count endemics species
            if endemics == "No origen or endemisme column":
                total_en: str = endemics
            else:
                total_en: int = count_en

            # Write out the register file.
            uti.registry_json(
                REGISTER_DIRPATH,
                file_name,
                species,
                authors_log,
                total_en,
                title_header,
            )

            # To see where the function goes by terminal -> useful when parsing many files at once.
            print(f"Done with {file_name}")

    return f"Done writing all {count_files} json files!"


# --------------------------------------------------------------------------------------------------------------------
# --- JSON MODIFICATION  --- #
# --------------------------------------------------------------------------------------------------------------------
def files_revison() -> str:
    """
    Review al the files created and modify it if necessary.
    """
    # Make sure there is no double keys, headers
    uti.modify_json_files(JSON_DIRPATH)
    # Change the data scheme in the jsons
    uti.order_data()
    # List of all the json
    json_list: list[str] = uti.files_extractor(JSON_DIRPATH, "json")
    # Add TaxId
    uti.add_taxid_to_species(JSON_DIRPATH, json_list)
    # Add GbifId
    uti.add_gbifID_to_species(JSON_DIRPATH, json_list)
    # Add key Familia
    uti.add_family_to_species(JSON_DIRPATH, json_list)
    # Add key Classe+
    uti.add_class_to_species(JSON_DIRPATH, json_list)
    # Add key Kingdom
    uti.add_kingdom_to_species(JSON_DIRPATH, json_list)
    # Insert url images
    uti.add_images_to_database(JSON_DIRPATH, json_list)
    # Validate and rewrite links images
    uti.validate_urls_in_json_files(json_list)
    uti.rewrite_urls_in_json_files(json_list)
    uti.add_uicn_gbif_to_species(JSON_DIRPATH, json_list)

    return f"All files checked out and modified"


# --------------------------------------------------------------------------------------------------------------------
# --- DATABASE SET UP --- #
# --------------------------------------------------------------------------------------------------------------------


def database_structure() -> str:
    """
    Groups together all database-related functions
    """

    # List all the items in every json.
    itemsdb: list[dict] = uti.get_latest_items_from_json_files(JSON_DIRPATH)

    # Insert the item in the database.
    uti.insert_or_update_items(itemsdb)
    # #Creates collection for register
    uti.add_collection_from_json(
        REGISTER_DIRPATH, "Register.json", "registry", "file_name"
    )
    # #Creates collection for Footers
    uti.add_collection_from_json(REGISTER_DIRPATH, "Footers.json", "footers", "file")

    return f"Done with the database!"


# --------------------------------------------------------------------------------------------------------------------
# --- SUMMARY FILE --- #
# --------------------------------------------------------------------------------------------------------------------


def summary_creation() -> str:
    """
    Create summary file for better comprehension of all the data.
    """
    uti.count_taxid_and_imgs(JSON_DIRPATH, REGISTER_DIRPATH)
    # Summary data of the register.json file
    uti.sum_num_species(REGISTER_DIRPATH, "Register.json")
    # Summary data of the database
    uti.update_tax_id_count_in_json(REGISTER_DIRPATH, "Summary.json")

    return f"The summary has been updated."


# ----------------------------------------------------------------------------------------------------------------------------------------------------------------
# --- MAIN --- #
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------


def main() -> str:
    """
    Recalls all the functions above to simplify the execution.
    """

    #print(file_parser())
    #print(files_revison())
    print(database_structure())
    print(summary_creation())

    return f"Done with this script!"


if __name__ == "__main__":

    print()
    print("Running the script...")
    print()
    print("This could take some time...")
    print()
    print(main())
