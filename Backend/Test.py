import pytest
import os
import json
from unittest.mock import patch, MagicMock
from utils import separate_autor, search_images_in_gbif, files_extractor, NoMatchingFilesError, get_species_info, extract_items_from_json_files, get_gbifId_of_GBIF



@pytest.fixture
def sample_list1():
    return ["Canis Familiaris John Doe", "Canis Lupus Jane Smith", "Felis silvestre Alice Wonderland"]

@pytest.fixture
def sample_list2():
    return ["Canis Familiaris", "Canis Lupus", "Felis silvestre"]

def test_separate_autor(sample_list1, sample_list2):
    result = separate_autor(sample_list1, sample_list2)
    expected_result = ["John Doe", "Jane Smith", "Alice Wonderland"]
    assert result == expected_result

def test_search_images_in_gbif():
    # Test with a valid species name
    species_name_valid = "Panthera leo"
    result_valid = search_images_in_gbif(species_name_valid)
    assert isinstance(result_valid, list)

    # Test with an invalid or non-existent species name
    species_name_invalid = "InvalidSpeciesName123"
    result_invalid = search_images_in_gbif(species_name_invalid)
    assert result_invalid == []

    # Test with an empty species name
    species_name_empty = None
    result_empty = search_images_in_gbif(species_name_empty)
    assert result_empty == []   

@pytest.fixture
def temp_directory(tmpdir):
    # Create temporary directory for testing
    temp_dir = tmpdir.mkdir("test_files")
    return str(temp_dir)

def test_files_extractor_json(temp_directory):
    # Create some JSON files in the temporary directory
    json_files = ["file1.json", "file2.json", "not_a_json.txt"]
    for file in json_files:
        temp_file = os.path.join(temp_directory, file)
        with open(temp_file, "w") as f:
            f.write("")

    # Call the files_extractor function for JSON files
    result = files_extractor(temp_directory, "json")

    # Assert that the result contains only JSON files
    expected_result = ["file1.json", "file2.json"]
    assert result == expected_result

def test_files_extractor_excel(temp_directory):
    # Create some Excel files in the temporary directory
    excel_files = ["file1.xlsx", "file2.xlsx", "not_an_excel.txt"]
    for file in excel_files:
        temp_file = os.path.join(temp_directory, file)
        with open(temp_file, "w") as f:
            f.write("")

    # Call the files_extractor function for Excel files
    result = files_extractor(temp_directory, "excel")

    # Assert that the result contains only Excel files
    expected_result = ["file1.xlsx", "file2.xlsx"]
    assert result == expected_result

def test_files_extractor_no_matching_files(temp_directory):
    # Call the files_extractor function for a non-existing file type
    with pytest.raises(NoMatchingFilesError):
        files_extractor(temp_directory, "pdf")   


def test_get_species_info_species():
    
    # Call the get_species_info function
    result = get_species_info('Homo sapiens', 'class', 'specie')

    # Assert that the result is the expected species name
    assert result == 'Mammalia'


def test_get_species_info_taxid():
  
    # Call the get_species_info function
    result = get_species_info('9606 ', 'family', 'taxid')

    # Assert that the result is the expected species name
    assert result == 'Hominidae'

def test_get_species_info_no_result():

    # Call the get_species_info function
    result = get_species_info('dfsdfhsdj', 'kingdom', 'specie')

    # Assert that the result is None
    assert result is None 

@pytest.fixture
def sample_json_files(tmpdir):
    # Create temporary directory for testing
    temp_dir = tmpdir.mkdir("json_files")

    # Create some JSON files in the temporary directory
    json_files = ["file1.json", "file2.json", "file3-c1.json", "file4-c2.json"]
    for file in json_files:
        temp_file = temp_dir.join(file)
        with open(temp_file, "w") as f:
            json.dump([{"item": f"Item from {file}"}], f)

    return temp_dir

def test_extract_items_from_json_files(sample_json_files):
    # Get the path and JSON files from the fixture
    path = str(sample_json_files)
    json_files = [f.basename for f in sample_json_files.listdir()]

    # Call the extract_items_from_json_files function
    result = extract_items_from_json_files(path, json_files)

    # Assert that the result contains items from all JSON files
    expected_result = [
        {"item": "Item from file1.json"},
        {"item": "Item from file2.json"},
        {"item": "Item from file3-c1.json"},
        {"item": "Item from file4-c2.json"},
    ]
    assert result == expected_result  


def test_get_gbifId_of_GBIF():
   

    # Call the function with a valid species name
    specie_name_valid = "Canis lupus chanco"
    result_valid = get_gbifId_of_GBIF(specie_name_valid)

    # Assert that the result is the expected gbifID
    assert result_valid == "4420680897"

   
    specie_name_invalid = "InvalidSpeciesName123"
    result_invalid = get_gbifId_of_GBIF(specie_name_invalid)

    # Assert that the result is None
    assert result_invalid is None


if __name__ == '__main__':
    pytest.main()