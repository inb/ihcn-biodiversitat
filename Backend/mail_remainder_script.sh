#!/bin/bash

# Define the base directory relative to where the script is located
BASE_DIR="$(dirname "$(dirname "$(realpath "$0")")")"

# Log file for cron job errors
LOG_FILE="$BASE_DIR/cron_error.log"

# Log current date and time
echo "Script run at: $(date)" >> "$LOG_FILE"

# Log the base directory
echo "Base directory: $BASE_DIR" >> "$LOG_FILE"

# Change to the base directory
cd "$BASE_DIR" || { echo "Failed to change directory to $BASE_DIR" >> "$LOG_FILE"; exit 1; }

# Log the current working directory
echo "Current working directory after cd: $(pwd)" >> "$LOG_FILE"

# Load environment variables from .env file
ENV_FILE="$BASE_DIR/.env"
if [ -f "$ENV_FILE" ]; then
    source "$ENV_FILE"
else
    echo "Environment file not found: $ENV_FILE" >> "$LOG_FILE"
    exit 1
fi

# Log loaded environment variables
echo "Loaded environment variables:" >> "$LOG_FILE"
env >> "$LOG_FILE"

# Define the Conda environment path from the environment variable
if [ -z "$CONDA_ENV_PATH" ]; then
    echo "CONDA_ENV_PATH is not set in the environment. Using default path: $HOME/anaconda3/" >> "$LOG_FILE"
    CONDA_ENV_PATH="$HOME/anaconda3/"
fi

# Activate the Conda environment
CONDA_ACTIVATE_SCRIPT="$CONDA_ENV_PATH/etc/profile.d/conda.sh"
if [ -f "$CONDA_ACTIVATE_SCRIPT" ]; then
    source "$CONDA_ACTIVATE_SCRIPT"
    conda activate "$CONDA_ENV_NAME" >> "$LOG_FILE" 2>&1
else
    echo "Conda activation script not found: $CONDA_ACTIVATE_SCRIPT" >> "$LOG_FILE"
    exit 1
fi

# Log the current working directory
echo "Current working directory after Conda activation: $(pwd)" >> "$LOG_FILE"

# Execute the Python script
PYTHON_SCRIPT="$BASE_DIR/Backend/API_Google/check_online.py"
if [ -f "$PYTHON_SCRIPT" ]; then
    python "$PYTHON_SCRIPT" >> "$LOG_FILE" 2>&1
else
    echo "Python script not found: $PYTHON_SCRIPT" >> "$LOG_FILE"
    exit 1
fi
