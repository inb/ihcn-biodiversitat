const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/biodiversitat/',
  transpileDependencies: true
})
