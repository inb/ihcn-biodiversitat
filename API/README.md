# IHCN-Biodiversitat

A project with researchers in Catalonia to catalogue its biodiversity and make a web application.

---
---
## FLASK API

Explanation of the function of the flask api that connects with the mongodb and retrive data with endpoints.

---
---
## <u>INDEX</u>

INTRODUCTION:
- [Files involved](#listfiles)
    - [Files](#listscripts)

THE CODE :
- [Description](#descripcion)
- [Before Starting](#before-start)
- [Libraries](#liblist)

---
---

## <a id="listfiles">Files of this repository</a> 

### <a id="listscripts">_Files_</a> 

- [api-requirements.txt](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/API/api-requirements.txt) -> Requirements that need the yml to create the conatiner and run the app.

- [Bio_API.py](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/API/Bio_API.py) -> Flask app script.
  
- [Dockerfile](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/API/Dockerfile) -> Configuration to mount a docker container with the app.
  
- [README.md](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/API/README.md) -> This file.

# THE API:

## <a id="descripcion">Description</a> 
This part of the project contains the connection to the mongodb and the retrive of querys.

There is only done the getters.

---
## <a id="before-start">Before starting</a> 

Now the API is coded to run with the [docker-compose.yml](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/docker-compose.yml), but if is needed to run it individually could be done.

Just have the database up, change the route to the connection if needed.

With the conda enviroment created in the [Backend](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend) will not appear problems. The [api-requirements.txt](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/API/api-requirements.txt) is for pip install the libraries in the container.


## <a id="liblist">Used libraries</a> 
- Flask, Flask_cors
- MongoClient
- Et3, Entrez, os, re
  



