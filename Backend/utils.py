# --------------------------------------------------------------------------------------------------------------------
# --- IMPORTS --- #
# --------------------------------------------------------------------------------------------------------------------

import os
import re
import json
import pymongo
import requests
import configparser
from requests_html import HTMLSession

from pathlib import Path
from Bio import Entrez
from ordered_set import OrderedSet
from urllib.parse import urlparse, ParseResult
from bs4 import BeautifulSoup
from dotenv import load_dotenv


# --------------------------------------------------------------------------------------------------------------------
# --- GLOBAL VARIABLES --- #
# --------------------------------------------------------------------------------------------------------------------
# Get the path to the directory containing the script
script_directory = os.path.dirname(os.path.abspath(__file__))
os.chdir(script_directory)
# Get the path to the config.ini file
config_file_path = os.path.join(script_directory, "Configuration", "config.ini")
config = configparser.ConfigParser()
config.read(config_file_path)
load_dotenv(dotenv_path=os.path.join(os.path.dirname(__file__), "..", ".env"))

# MongoDB connection details
host = os.getenv("MONGODB_HOST")
database = os.getenv("MONGODB_DATABASE")
username = os.getenv("MONGODB_USERNAME")
password = os.getenv("MONGODB_PASSWORD")

CLIENT: str = (
    f"mongodb://{username}:{password}@{host}/{database}?authMechanism=SCRAM-SHA-1"
)
DATABASE: str = database
COLLECTION: str = config.get("DATABASE", "collection")

JSON_DIRPATH: str = config.get("ROUTES", "json_dirpath")

Entrez.email = config.get("ENTREZ", "email")
Entrez.api_key = config.get("ENTREZ", "api_key")


# --------------------------------------------------------------------------------------------------------------------
# --- DIRECTORY LISTING --- #
# --------------------------------------------------------------------------------------------------------------------
class NoMatchingFilesError(Exception):
    pass


def files_extractor(direct: str, file_type: str) -> list[str]:
    """
    Get all the names of the files of a directory
    Param directory to search, file_type to extract
    Return list of files
    """

    name_files: list[str] = os.listdir(direct)
    sorted_name_files: list[str] = sorted(name_files)

    # Store only the desired files
    name_fileList: list[str] = []

    for name in sorted_name_files:

        # For the excels files
        if file_type.lower() == "json" and name.lower().endswith(".json"):
            name_fileList.append(name)
        # For the json files
        elif file_type.lower() == "excel" and (
            name.lower().endswith(".xlsx") or name.lower().endswith(".XLSX")
        ):
            name_fileList.append(name)

    if not name_fileList:
        raise NoMatchingFilesError(f"No {file_type} files found in the directory.")

    return name_fileList


# --------------------------------------------------------------------------------------------------------------------
# --- DISCOVERER MANIPULATION --- #
# --------------------------------------------------------------------------------------------------------------------
def num_common_chars(str1: str, str2: str) -> int:
    """
    Parse a two strings
    Parameters: str1 - string, str2 - string
    Returns the number of common chars between two strings
    """

    return sum(c1 == c2 for c1, c2 in zip(str1, str2))


# --------------------------------------------------------------------------------------------------------------------


# Option to use in most_similar_substring
def needleman_wunsch(
    str1: str,
    str2: str,
    match_score: int = 2,
    mismatch_score: int = -1,
    gap_penalty: int = -1,
) -> str:
    """
    Do a needleman alignment between two strings and get the best score
    Param: str1 - string, str2 - string (optional match_score, mismatch_score and gap_penalty)
    Returns: str The best score
    """

    # Inicializar matriz de puntajes
    n: int = len(str1)
    m: int = len(str2)

    scores: list[int] = [[0 for j in range(m + 1)] for i in range(n + 1)]

    # Inicializar primera fila y columna con penalización de gaps
    for i in range(1, n + 1):
        scores[i][0] = scores[i - 1][0] + gap_penalty
    for j in range(1, m + 1):
        scores[0][j] = scores[0][j - 1] + gap_penalty

    # Llenar matriz de puntajes
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            if str1[i - 1] == str2[j - 1]:
                match: int = match_score
            else:
                match: int = mismatch_score
            scores[i][j] = max(
                scores[i - 1][j - 1] + match,
                scores[i - 1][j] + gap_penalty,
                scores[i][j - 1] + gap_penalty,
            )

    # Calcular puntuación de similitud
    score: str = scores[n][m]

    return score


# --------------------------------------------------------------------------------------------------------------------


def most_similar_substring(main_str: str, substrings: list[str]) -> str:
    """
    Compares string with a list of substrings
    Parameters: main_str - string, substrings - list of strings
    Returns the most similar substring
    """

    best_score: int = -1
    best_substring: str = ""

    for substr in substrings:

        # Extract the common chars
        score: int = num_common_chars(main_str, substr)

        # Option 2 if needed
        # score = needleman_wunsch(main_str, substr)

        # Divide the substring by spaces
        parts: list[str] = substr.split()

        # If the substring is more than 3 words
        if len(parts) > 3:
            if score > best_score:
                if parts[3] not in main_str:
                    continue
                else:
                    if len(parts) > 4:
                        if parts[4] not in main_str:
                            continue
                        else:
                            best_score: int = score
                            best_substring: str = substr

                    else:
                        best_score: int = score
                        best_substring: str = substr

        # If the substring is only 1 word
        elif len(parts) == 1:
            if score > best_score:
                best_score: int = score
                best_substring: str = substr
        else:
            if score > best_score:
                if parts[1] not in main_str:
                    continue
                else:
                    best_score: int = score
                    best_substring: str = substr

    return best_substring


# --------------------------------------------------------------------------------------------------------------------


# In case is need it
def get_difference(lista1: list[str], lista2: list[str]) -> list[str]:
    """
    Compares from two list the differences between each string and returns the difference
    """
    diferences: list[str] = []
    # Handle when have x Godfey
    # Check againg with normal funciton
    for index, discoverer in enumerate(lista1):
        first_letter: str = discoverer[0][0]
        if not first_letter.isupper():
            lista1[index] = discoverer[1:]

    for s1, s2 in zip(lista1, lista2):
        # if not s1[0][0].isupper():
        # 	s1[0] = s1[1:]
        sub_s1 = OrderedSet(s1.split())
        sub_s2 = OrderedSet(s2.split())
        diff = sub_s1 - sub_s2
        diferences.append(" ".join(diff))
    return diferences


# --------------------------------------------------------------------------------------------------------------------


def separate_autor(list1: list[str], list2: list[str]) -> list[str]:
    """
    Compares two list of strings.
    Params :
            list1 -> list of strings to separte
            list2 -> list of substrings to compare
    Returns the non-matching substring in list1 aka authors.
    """
    authors: list[str] = []

    for str1 in list1:
        # Split the string into word to get the author
        to_search: list[str] = str1.split()
        # Start the count to posible matches between species
        count: int = 0
        # Store all the species matched
        all_matches: list[str] = []
        # Store all the words that not matched
        au: list[str] = []

        for str2 in list2:
            # Check if the first word matches with any specie in list2
            if to_search[0] in str2:
                count += 1
                all_matches.append(str2)
            else:
                continue

        # If the is more than one match
        if count > 1:

            # Compare with the best match
            best_substring: str = most_similar_substring(str1, all_matches)

            for word in to_search:
                if word not in best_substring:
                    au.append(word.strip())

            one_author: str = " ".join(item for item in au)
            authors.append(one_author)

        # If there is a match
        elif count == 1:

            for word in to_search:
                if word not in all_matches[0]:
                    au.append(word.strip())

            one_author: str = " ".join(item for item in au)
            authors.append(one_author)

    return authors


# --------------------------------------------------------------------------------------------------------------------
# --- SPECIE COLUMN CLEANING --- #
# --------------------------------------------------------------------------------------------------------------------


def get_only_specie(path: str, file: str) -> dict:
    """
    Save all the species names in italic
    Params, path of the files, file the name of the file
    Return dict with key name file and value colum species
    """

    with Path(path):

        name_file: str = Path(path + "/" + file).stem

        f: object = open(path + "/" + file)
        soup: BeautifulSoup = BeautifulSoup(f, "lxml")

        species: list[str] = []

        # Search for the column that is in italic format
        if soup.find("td"):
            for p in soup.select("td p"):
                si: list[str] = p.find_all("i")
                output: str = " ".join(
                    [
                        item.text.strip()
                        .replace("\n", " ")
                        .replace("\t\t\t", "")
                        .replace("\t", "")
                        for item in si
                    ]
                )
                species.append(output)
        else:
            for p in soup.select("p"):
                si: list[str] = p.find_all("i")
                output: str = " ".join(
                    [
                        item.text.strip()
                        .replace("\n", " ")
                        .replace("\t\t\t", "")
                        .replace("\t", "")
                        for item in si
                    ]
                )
                species.append(output)

        # Check if there is empty string in list
        res: bool = any(len(ele) == 0 for ele in species)

        # Brake code if not all species in italic format
        if res:
            return f"Not all the species where in italic check the docs."
        else:
            return {name_file: species}


# --------------------------------------------------------------------------------------------------------------------


def extract_species_name(species: list[str]) -> list[str]:
    """
    Extract the parenthesis of a specie
    Renderization to search better the taxonomy Ids in NCBI
    Param: List of species without filter
    Return: List of species optimiced
    """

    result: list[str] = []

    # Loop through species if have parenthesis
    for specie in species:
        species_name: str = re.sub(
            r"\([^)]*\)", "", specie
        ).strip()  # Remove text inside parenthesis
        result.append(species_name)
    return result


# --------------------------------------------------------------------------------------------------------------------


def extract_species_name_extra(specie: str) -> str:
    """
    If the specie have the last two repeated.
    Only is used if the taxid is not found at first
    Parameter: specie string
    Return specie without repetition.
    """

    words: list[str] = specie.split()  # Split species name into words
    if len(words) > 2 and words[-1] == words[-2]:  # Check if last two words are same
        specie_name: str = " ".join(words[:-1])
    else:
        specie_name: str = specie

    return specie_name


# --------------------------------------------------------------------------------------------------------------------
# --- NCBI API ---#
# --------------------------------------------------------------------------------------------------------------------


def taxonomyID(nameEspecies: list[str]) -> list:
    """
    Extracts the taxonomy ID from the name of the species.
    Optimized to found a taxId.
    Param: list of strings with the name of the species
    Return: list with the taxonomy ID of the species.
    """

    taxonomyId: list[str] = []

    species: str = extract_species_name(nameEspecies)

    for name in species:

        # Search with all extra words founded in the specie name
        if "subsp." in name:
            name: str = re.sub("subsp.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "nothosubsp." in name:
            name: str = re.sub("nothosubsp.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "cf." in name:
            name: str = re.sub("cf.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "ssp." in name:
            name: str = re.sub("ssp.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "sp." in name:
            name: str = re.sub("sp.", "", name)
            name: str = re.sub("indet.", "", name)
            name: str = re.sub("gen.", "", name)
            name: str = re.sub("aff.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "sp1" in name:
            name: str = re.sub("sp1", "", name)
            name: str = re.sub("gr.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "sp2" in name:
            name: str = re.sub("sp2", "", name)
            name: str = re.sub("gr.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "var." in name:
            name: str = re.sub("var.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        elif "gr." in name:
            name: str = re.sub("gr.", "", name)
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

        else:
            handle = Entrez.esearch(db="taxonomy", term=f"{name}[title]")
            record = Entrez.read(handle)
            if record["IdList"] == []:
                # If not found try:
                new_name: str = extract_species_name_extra(name)
                handle = Entrez.esearch(db="taxonomy", term=f"{new_name}[title]")
                record = Entrez.read(handle)
                taxonomyId.append(record["IdList"])
                handle.close()
            else:
                taxonomyId.append(record["IdList"])
                handle.close()

    return taxonomyId


# --------------------------------------------------------------------------------------------------------------------


def get_species_info(to_search: str, field: str, search_type: str) -> str | None:
    """
    Retrieve the family, kingdom or classe of a species using the NCBI API.
    Parameters:
            - to_search: search term (species name, taxid or family name)
            - field: field where to search (class, family, or kingdom)
            - search_type: type of search term (species, taxid, or family)
    Returns:
            - scientific name of the specified field or None if not found
    """

    # Check search type and retrieve taxid
    if search_type == "specie" or search_type == "family" or search_type == "class":
        handle = Entrez.esearch(db="taxonomy", term=to_search)
        record = Entrez.read(handle)
        if record["Count"] == "0":
            return None
        taxid = record["IdList"][0]
    elif search_type == "taxid":
        taxid = to_search
    else:
        return None

    # Retrieve record for the taxid
    handle = Entrez.efetch(db="taxonomy", id=taxid, retmode="xml")
    record = Entrez.read(handle)

    # Extract the name of the specified field
    if field == "title":
        return record[0]["ScientificName"]

    for lineage in record[0]["LineageEx"]:
        if lineage["Rank"] == field:
            return lineage["ScientificName"]
    return None


# --------------------------------------------------------------------------------------------------------------------
# --- GET THE AUTHOR AND CREATION DATE OF EXCELS --- #
# --------------------------------------------------------------------------------------------------------------------
def get_author_creation_date(title_header: list[str]) -> tuple[str, str]:
    """
    Retrieves the author and creation date of the Excels, found in title_header.
    Parameters:
            - title_header: list[str] with the header information of each excel.
    Returns:
            - author and creation date of the Excel file
    """

    # In case title_header is empty, returns empty string
    if not title_header:  # Si title_header está vacío
        return "", ""

    # Replace the \n with a space from the title_header.
    title_header_clean: list[str] = []

    for info in title_header:
        title_header_clean.append(info.replace("\n", " ").strip())

    # Get author and creation date from the title_header
    for item in title_header_clean:

        # Separate the content
        portions = re.split(r"\s*[-–]\s*", item)

        # Get author from the separator
        author: str = " - ".join(portions[1:-1])
        # Avoid multiple spaces between words
        author_c: str = re.sub(r"\s+", " ", author)
        # Get creation date from the separator
        creation_date: str = str(portions[-1])
        # Avoid multiple spaces between words
        creation_date_c: str = re.sub(r"\s+", " ", creation_date)

        return author_c, creation_date_c


# --------------------------------------------------------------------------------------------------------------------
# --- RESULTS FILES --- #
# --------------------------------------------------------------------------------------------------------------------


def registry_json(
    filepath: str,
    file_name: str,
    num_species: int,
    num_discoverers: int,
    count_endemic: int | str,
    title_header: list[str],
) -> None:
    """
    Creates a register json with data from the excels
    Parameters: filepath: str, file_name: str, num_species: int, num_discoverers: int,
                            taxId_found:int, percent_taxid: float, count_endemic: int|str
    """

    # If the file exists read it, if not create a variable
    try:
        with open(filepath + "Register.json", "r") as file:
            data = json.load(file)

    except FileNotFoundError:
        data: list[dict] = []

    # Get authors and creation dates
    author, creation_date = get_author_creation_date(title_header)

    # Ensure author and creation_date are not empty
    if not author:
        author = None
    if not creation_date:
        creation_date = None

    # Data structure

    item: dict = {
        "file_name": file_name,
        "num_species": num_species,
        "num_discoverers": num_discoverers,
        "count_endemic": count_endemic,
        "author": author,
        "creation_date": creation_date,
        # 'total_GbifId': total_GbifId,
        # 'total_GbifId_None': total_GbifId_None,
    }

    # Check if the excel have been parsed
    existing_item = next((i for i in data if i["file_name"] == file_name), None)

    if existing_item:
        # If exist modify it
        existing_item.update(item)
    else:
        # If not add it to the json
        data.append(item)

    # Sort the JSON data items by the first string key
    sorted_data: list[dict] = sorted(data, key=lambda x: x.get("file_name", "").lower())

    # Writes the register json
    with open(filepath + "Register.json", "w") as file:
        json.dump(sorted_data, file, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------


def modify_json_files(directory: str) -> None:
    """
    Check from a list of json if there are keys with "/"
    If there are split the key in two parts
    Parameter: directory where to look for
    """
    replace_key_in_json_files(
        JSON_DIRPATH,
        "Referència bibliogràfica  http://www.aula2005.com/html/anfipodos/",
        "Referència bibliogràfica www.aula2005.com/html/anfipodos",
    )
    replace_key_in_json_files(
        JSON_DIRPATH, "Directiva hàbitats", "Directiva Hàbitats 92/43/CEE"
    )
    replace_key_in_json_files(
        JSON_DIRPATH, "Directiva Hàbitats", "Directiva Hàbitats 92/43/CEE"
    )
    replace_key_in_json_files(
        JSON_DIRPATH, "Decret 328/1992 (PEIN)", "Decret 328/1992 PEIN"
    )
    replace_key_in_json_files(
        JSON_DIRPATH, "CITES", "Conveni comerç internacional (CITES)"
    )
    replace_key_in_json_files(JSON_DIRPATH, "Categoria UICN (Europa)", "UICN (Europa)")
    replace_key_in_json_files(JSON_DIRPATH, "Categoria UICN (Global)", "UICN (Global)")
    replace_key_in_json_files(
        JSON_DIRPATH, "Categoria UICN (Mediterrània)", "UICN (Mediterrani)"
    )
    replace_key_in_json_files(JSON_DIRPATH, "Estatus Catalunya", "Estatus a Catalunya")
    replace_key_in_json_files(
        JSON_DIRPATH,
        "Invertebrats que requereixen mesures de conservació a Catalunya (ICHN,  2008)",
        "Invertebrats que requereixen mesures de conservació a Catalunya (ICHN, 2008)",
    )

    # Search in the directory
    for filename in os.listdir(directory):
        if filename.endswith(".json"):
            filepath = os.path.join(directory, filename)
            with open(filepath, "r") as file:
                data = json.load(file)
            modified_data = []
            for obj in data:
                modified_obj = {}
                for key, value in obj.items():
                    if "Família/" in key:
                        key_parts = key.split("/")
                        for i, part in enumerate(key_parts):
                            if i == 0:
                                modified_obj[part] = value.split("/")[0]
                            else:
                                modified_obj[part.capitalize()] = value.split("/")[i]
                    else:
                        modified_obj[key] = value
                modified_data.append(modified_obj)
            with open(filepath, "w") as file:
                json.dump(modified_data, file, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------
def add_taxid_to_species(path: str, json_files: list[str]) -> None:
    """
    Adds the taxId if not exists in the document
    Parameters: path where json files should be
                            json files list of all jsons
    """

    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"TaxId to {file}")

        taxid: str = None
        specie: str = None

        for spe in objects:
            taxonomia = spe.get("TAXONOMIA", {})
            taxId_value = taxonomia.get("TaxId", {})
            spe_value = taxonomia.get("Espècie", {})

            # Checks if TaxId exists
            if taxId_value and taxId_value != None:
                # Exists and not null
                continue
            else:
                specie: str = [spe_value]
                taxid: str = taxonomyID(specie)

            if taxid[0] == []:
                spe["TAXONOMIA"]["TaxId"] = None
            else:
                spe["TAXONOMIA"]["TaxId"] = taxid[0][0]

            # Write the updated JSON file
            with open(f"{path}/{file}", "w") as f:
                json.dump(objects, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------


def add_family_to_species(path: str, json_files: list[str]) -> None:
    """
    Adds the family if not exists in the document
    Standard the key familia fisrt
    Checks first from taxid if not from first word of specie
    """

    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"Family to {file}")

        # Variables
        taxid: str = None
        family: str = None
        specie: str = None

        for spe in objects:
            taxonomia = spe.get("TAXONOMIA", {})
            fake_familia_value = taxonomia.get("Familia")
            familia_value = taxonomia.get("Família")

            # Replace incorrect key names
            if fake_familia_value is not None:
                taxonomia["Família"] = taxonomia.pop("Familia")
                familia_value = taxonomia.get("Família")
            # Checks if Familia exists
            if familia_value and familia_value is not None:
                # Exists and not null
                family: str = familia_value.split()
                if len(family) > 1:
                    if "Naviculales" in family:
                        pass
                    elif "Demospongiae" in family:
                        pass
                    elif "Bacillariophyta" in family:
                        pass
                    elif "Incertae" in family:
                        pass
                    else:
                        familia_value = family[0]

            elif familia_value is None:
                if taxonomia["TaxId"] != None:
                    # If taxId is present obtain family
                    taxid: str = taxonomia["TaxId"]
                    familia_value: str = get_species_info(taxid, "family", "taxid")
                else:
                    # Search family from specie
                    if "(" in taxonomia["Espècie"]:
                        specie: str = taxonomia["Espècie"].split()[0]
                        familia_value: str = get_species_info(
                            specie, "family", "specie"
                        )

                    else:
                        spe_split: list[str] = taxonomia["Espècie"].split()

                        if len(spe_split) > 1:
                            specie: str = (
                                taxonomia["Espècie"].split()[0]
                                + " "
                                + taxonomia["Espècie"].split()[1]
                            )
                            familia_value: str = get_species_info(
                                specie, "family", "specie"
                            )
                            if not family:
                                specie: str = taxonomia["Espècie"].split()[0]
                                familia_value: str = get_species_info(
                                    specie, "family", "specie"
                                )
                        else:
                            specie: str = taxonomia["Espècie"].split()[0]
                            familia_value: str = get_species_info(
                                specie, "family", "specie"
                            )

                # If family retrieved
                if familia_value:
                    spe["TAXONOMIA"]["Família"] = familia_value
                else:
                    spe["TAXONOMIA"]["Família"] = None

            # Write the updated JSON file
            with open(f"{path}/{file}", "w") as f:
                json.dump(objects, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------
def add_class_to_species(path: str, json_files: list[str]) -> None:
    """
    Add The Class to the documents
    Retrieves the family if exist or the first word of the specie if do not.
    """

    for file in json_files:

        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"Class to {file}")

        for spe in objects:

            taxonomia = spe.get("TAXONOMIA", {})
            classe_value = taxonomia.get("Classe")

            classe: str = None
            taxid: str = None
            specie: str = None
            familia: str = None

            # Retrive the Classe of every specie
            if classe_value and classe_value != None:
                # If classe is present then continue
                continue
            else:

                if taxonomia["TaxId"] != None:
                    # If taxId is present obtain classe
                    taxid: str = taxonomia["TaxId"]
                    classe_value: str = get_species_info(taxid, "class", "taxid")

                else:
                    # If not present then obtain it from the Família key
                    if taxonomia["Família"] and taxonomia["Família"] != None:
                        # Sometimes familia comes with discoveres
                        # Just get the first word that belongs to the familia
                        familia: str = taxonomia["Família"].split()[0]
                        classe_value: str = get_species_info(familia, "class", "family")
                        if not classe_value:
                            # If the Família key is not present use the specie instead
                            if "(" in taxonomia["Espècie"]:
                                specie: str = taxonomia["Espècie"].split()[0]
                                classe_value: str = get_species_info(
                                    specie, "class", "specie"
                                )

                            else:
                                specie: str = (
                                    taxonomia["Espècie"].split()[0]
                                    + " "
                                    + taxonomia["Espècie"].split()[1]
                                )
                                classe_value: str = get_species_info(
                                    specie, "class", "specie"
                                )
                                if not classe_value:
                                    specie: str = taxonomia["Espècie"].split()[0]
                                    classe_value: str = get_species_info(
                                        specie, "class", "specie"
                                    )
                    else:
                        # If the Família key is not present use the specie instead
                        if "(" in taxonomia["Espècie"]:
                            specie: str = taxonomia["Espècie"].split()[0]
                            classe_value: str = get_species_info(
                                specie, "class", "specie"
                            )

                        else:
                            spe_split: list[str] = taxonomia["Espècie"].split()

                            if len(spe_split) > 1:
                                specie: str = (
                                    taxonomia["Espècie"].split()[0]
                                    + " "
                                    + taxonomia["Espècie"].split()[1]
                                )
                                classe_value: str = get_species_info(
                                    specie, "class", "specie"
                                )
                                if not classe_value:
                                    specie: str = taxonomia["Espècie"].split()[0]
                                    classe_value: str = get_species_info(
                                        specie, "class", "specie"
                                    )
                                else:
                                    specie: str = taxonomia["Espècie"].split()[0]
                                    classe_value: str = get_species_info(
                                        specie, "class", "specie"
                                    )

                if classe_value:
                    spe["TAXONOMIA"]["Classe"] = classe_value
                else:
                    spe["TAXONOMIA"]["Classe"] = None

            # Write the updated JSON file
            with open(f"{path}/{file}", "w") as f:
                json.dump(objects, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------
def add_kingdom_to_species(path: str, json_files: list[str]) -> None:
    """
    Add The Kingdom to the documents
    Retrieves the taxid if exist or the first word of the specie if do not.
    """
    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"Kingdom to {file}")

        for spe in objects:
            taxonomia = spe.get("TAXONOMIA", {})
            regne_value = taxonomia.get("Regne")

            kingdom: str = None
            taxid: str = None
            specie: str = None
            classe: str = None
            family: str = None

            # Retrive the Kingdom of every specie
            if regne_value:  # and spe["Regne"] != None
                # If kingdom is present then continue
                continue
            else:

                if taxonomia["TaxId"] != None:
                    # If taxId is present obtain kingdom
                    taxid: str = taxonomia["TaxId"]
                    regne_value: str = get_species_info(taxid, "kingdom", "taxid")

                elif taxonomia["Classe"] != None:
                    # If classe is present obtain kingdom
                    classe: str = taxonomia["Classe"]
                    regne_value: str = get_species_info(classe, "kingdom", "class")

                elif taxonomia["Família"] != None:
                    # If family is present obtain kingdom
                    family: str = taxonomia["Família"]
                    regne_value: str = get_species_info(family, "kingdom", "family")
                else:
                    # If not present then obtain it from the specie key
                    # Sometimes there is no class field or family

                    if "(" in taxonomia["Espècie"]:
                        specie: str = taxonomia["Espècie"].split()[0]
                        regne_value: str = get_species_info(specie, "kingdom", "specie")

                    else:
                        specie: str = (
                            taxonomia["Espècie"].split()[0]
                            + " "
                            + taxonomia["Espècie"].split()[1]
                        )
                        regne_value: str = get_species_info(specie, "kingdom", "specie")
                        if not regne_value:
                            specie: str = taxonomia["Espècie"].split()[0]
                            regne_value: str = get_species_info(
                                specie, "kingdom", "specie"
                            )

                if regne_value:
                    spe["TAXONOMIA"]["Regne"] = regne_value
                else:
                    spe["TAXONOMIA"]["Regne"] = None

            # Sort the keys within each object
            sorted_data: list[dict] = []
            for obj in objects:
                sorted_obj: dict = {key: obj[key] for key in sorted(obj.keys())}
                sorted_data.append(sorted_obj)

            # Write the updated JSON file
            with open(f"{path}/{file}", "w") as f:
                json.dump(sorted_data, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------


def count_taxid_and_imgs(directory_path: str, registry_path: str) -> None:
    """
    Count and write the taxid found in evry json file of each specie
    Parameters: - directory_path where all json files are stored
                            - registry_path where the registry file is stored
    """

    with open(registry_path + "Register.json") as file:
        registry = json.load(file)

    for file_obj in registry:
        filename: str = file_obj["file_name"]
        file_path: str = os.path.join(directory_path, filename + ".json")

        try:
            with open(file_path) as file:
                data = json.load(file)
                taxids: int = sum(
                    1 for obj in data if obj["TAXONOMIA"].get("TaxId") is not None
                )
                images: int = sum(
                    1
                    for obj in data
                    if obj["INFORMACIÓ COMPLEMENTÀRIA"].get("Image_urls") is not None
                )
                file_obj["taxId_found"] = taxids
                file_obj["valid_images"] = images
                file_obj["percent_taxid"] = (
                    f'{round((taxids/file_obj["num_species"] * 100) ,2)} %'
                )

        except FileNotFoundError:
            print(f"Error: File not found at path {file_path}")
            continue

    with open(registry_path + "Register.json", "w") as file:
        json.dump(registry, file, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------
# Before searching images
def remove_substrings(text: str) -> str:
    """
    Remove specified substrings from a given string.
    Parameters:
            - text: The original string.
    Returns:
            - The cleaned string with specified substrings removed.
    """
    # lsit of words to delete from the original string
    substrings: list[str] = [
        "subsp.",
        "nothosubsp.",
        "cf.",
        "ssp.",
        "sp.",
        "indet.",
        "gen.",
        "aff.",
        "sp1",
        "sp2",
        "gr.",
        "var.",
        "f.",
    ]

    for substring in substrings:
        text: str = text.strip().replace(substring, "")

    # Assure there is only one space between the words
    cleaned_text: str = re.sub(r"\s+", " ", text)

    return cleaned_text.strip()


# --------------------------------------------------------------------------------------------------------------------
# --- ADD IMAGES LINKS AND RELATED ---#
# --------------------------------------------------------------------------------------------------------------------


def search_images_in_wikimedia(species_name: str) -> list | None:
    """
    Search for images in wikimedia commons web
    Return an empty list if not found or a lsit with url
    """
    # Encode the species name for the URL
    encoded_species_name: str = requests.utils.quote(species_name)

    # Construct the search URL for Wikimedia Commons
    search_url = f"https://commons.wikimedia.org/w/index.php?search={encoded_species_name}&title=Special:MediaSearch&type=image"

    try:
        response: ParseResult = requests.get(search_url)
        response.raise_for_status()

        soup: BeautifulSoup = BeautifulSoup(response.content, "html.parser")
        image_urls: list[str] = []

        # Find all image elements in the search results
        image_elements = soup.find_all("img")
        for element in image_elements:
            if "data-src" in element.attrs:
                image_url = element["src"]
                image_urls.append(image_url)

        return image_urls

    except requests.exceptions.RequestException as e:
        print("An error occurred while accessing Wikimedia Commons:", str(e))

    return []


# --------------------------------------------------------------------------------------------------------------------
def search_images_in_gbif(species_name: str) -> list | None:
    """
    Get the url of an image if found media in gbif api, none otherwise
    """

    try:
        # Search for occurrences on GBIF API
        search_url = (
            f"https://api.gbif.org/v1/occurrence/search?scientificName={species_name}"
        )
        response = requests.get(search_url)
        occurrence_data = response.json()

        # Retrieve image URLs from occurrence records
        image_urls: list[str] = []
        for record in occurrence_data["results"]:
            if "media" in record:
                media = record["media"]
                if isinstance(media, list):
                    for media_item in media:
                        if "references" in media_item:
                            image_urls.append(media_item["references"])
                        elif "identifier" in media_item:
                            image_urls.append(media_item["identifier"])

        return image_urls

    except requests.exceptions.RequestException as e:
        print("An error occurred while accessing GBIF API:", str(e))

    return []



# --------------------------------------------------------------------------------------------------------------------


def add_images_to_database(path: str, json_files: list[str]) -> None:
    """
    Get the url of an image if found media in gbif api, none otherwise
    Update the database
    """
    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"IMG to {file}:")

        for obj in objects:
            store_img = obj.get("INFORMACIÓ COMPLEMENTÀRIA", {})
            images = store_img.get("Image_urls", {})
            taxonomia = obj.get("TAXONOMIA", {})

            # Retrive the Kingdom of every specie
            if images and images != None:
                # If img_url is present then continue
                continue
            else:

                taxid: str = taxonomia["TaxId"]

                if taxid:
                    # Retrieve scientific name from the obtained TaxID (NCBI taxid)
                    scientific_name: str = get_species_info(taxid, "title", "taxid")
                else:
                    specie_name: str = taxonomia["Espècie"]
                    scientific_name: str = remove_substrings(specie_name)

                # Checking via terminal everything ok
                print(scientific_name)

                if scientific_name:

                    # Search gbif
                    gbif: list[str] = search_images_in_gbif(scientific_name)
                    # Search wikkicommons
                    wikimedia: list[str] = search_images_in_wikimedia(scientific_name)
                  
                    if gbif:
                        obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = gbif

                    elif wikimedia:
                        obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = wikimedia

                    else:
                        obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = None

            # Write the updated JSON file
            with open(f"{path}/{file}", "w") as f:
                json.dump(objects, f, indent=4, ensure_ascii=False)

        print()


# --------------------------------------------------------------------------------------------------------------------


def extract_domains_from_json_files(file_paths: list[str]) -> None:
    """
    Extract all the unique domains stored in the json files
    Check the way they give results
    Parameters: file_paths: list of the json to check
    """
    unique_domains: list[str] = set()

    for file_path in file_paths:
        with open(JSON_DIRPATH + file_path, "r") as file:
            data: list[dict] = json.load(file)
            for obj in data:
                store_img = obj.get("INFORMACIÓ COMPLEMENTÀRIA", {})
                if store_img["Image_urls"]:
                    image_urls: list | None = store_img["Image_urls"]
                    if isinstance(image_urls, list):
                        for url in image_urls:
                            parsed_url: ParseResult = urlparse(url)
                            domain: str = parsed_url.netloc
                            if domain:
                                unique_domains.add(domain)

    for domain in unique_domains:
        print(f"https://{domain}")


# --------------------------------------------------------------------------------------------------------------------


def validate_urls_in_json_files(file_paths: list[str], timeout: int = 5) -> None:
    """
    Validate the existing links in a json returning only valids
    Parameters: file_paths: list of json files to validate the links,
                            timeout: int, the max number of seconds to wait a response

    """
    for file_path in file_paths:
        with open(JSON_DIRPATH + file_path, "r+") as file:
            data: list[dict] = json.load(file)
            modified: bool = False

            print(f"VALIDATING IMG to {file_path}:")

            for obj in data:
                store_img = obj.get("INFORMACIÓ COMPLEMENTÀRIA", {})

                if store_img["Image_urls"]:
                    print(obj["TAXONOMIA"]["Espècie"])
                    image_urls: list | None = store_img["Image_urls"]
                    if isinstance(image_urls, list):
                        validated_urls: list[str] = []
                        for url in image_urls:
                            if url is not None:
                                try:
                                    response: requests.Response | None = requests.head(
                                        url, timeout=timeout
                                    )
                                    if response.status_code == requests.codes.ok:
                                        validated_urls.append(url)
                                except requests.exceptions.RequestException:
                                    pass
                        if len(validated_urls) != len(image_urls):
                            obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = (
                                validated_urls if validated_urls else None
                            )
                            modified: bool = True

            if modified:
                file.seek(0)
                json.dump(data, file, indent=4, ensure_ascii=False)
                file.truncate()

            print()


# --------------------------------------------------------------------------------------------------------------------


def extract_image_url(img_tag: str) -> str | None:
    """
    Extract image link from image tag
    Parameters: img_tag, the iage tag containing the src image
    """

    image_url: None = None

    if img_tag:

        image_url: str = img_tag["src"]

    return image_url


# --------------------------------------------------------------------------------------------------------------------
def fetch_specific_image_url(url):
    """Fetch the specific image URL from the given page."""
    session = HTMLSession()
    response = session.get(url)
    response.html.render(timeout=20)  # Adjust timeout as necessary

    specific_img = response.html.find(
        "img.MuiCardMedia-root.MuiCardMedia-media.MuiCardMedia-img.il--ImageCard3__image--contain"
    )
    specific_img_url = specific_img[0].attrs.get("src") if specific_img else None
    return specific_img_url


# --------------------------------------------------------------------------------------------------------------------


def rewrite_urls_in_json_files(file_paths: list[str]) -> None:
    """ """
    for file_path in file_paths:
        with open(JSON_DIRPATH + file_path, "r+") as file:
            data: list[dict] = json.load(file)

            print(f"REWRITING IMG to {file_path}:")

            for obj in data:
                store_img = obj.get("INFORMACIÓ COMPLEMENTÀRIA", {})
                if store_img["Image_urls"]:
                    print(obj["TAXONOMIA"]["Espècie"])
                    image_urls: list | None = store_img["Image_urls"]
                    if isinstance(image_urls, list):
                        change_urls: list[str] = []
                        repited_url: list[str] = []
                        for url in image_urls:
                            if url.startswith("https://www.inaturalist.org/photos/"):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text

                                img_start_index: int = html_content.find(
                                    '<img class="medium photo" src="'
                                )
                                if img_start_index != -1:
                                    img_start_index += len(
                                        '<img class="medium photo" src="'
                                    )
                                    img_end_index: int = html_content.find(
                                        '"', img_start_index
                                    )
                                    img_url: str = html_content[
                                        img_start_index:img_end_index
                                    ]
                                    change_urls.append(img_url)
                                else:
                                    pass
                            elif url.startswith("http://bins.boldsystems.org"):
                                response = requests.get(url)
                                html_content = response.text
                                soup = BeautifulSoup(html_content, "html.parser")
                                img_tag = soup.select_one(
                                    ".ibox-content img[src^='//bins.boldsystems']"
                                )
                                if img_tag:
                                    img_url = img_tag["src"].lstrip("//")
                                    change_urls.append(img_url)
                                else:
                                    pass
                            elif url.startswith("https://minka-sdg.org/"):
                                response = requests.get(url)
                                html_content = response.text
                                soup = BeautifulSoup(html_content, "html.parser")
                                img_tag = soup.select_one(
                                    ".photocell img[src^='https://minka-sdg.org/']"
                                )
                                if img_tag:
                                    img_url = img_tag["src"]
                                    change_urls.append(img_url)
                                else:
                                    pass
                            elif url.startswith("https://www.izeltlabuak.hu/talalat/"):
                                specific_img_url = fetch_specific_image_url(url)
                                if specific_img_url:
                                    change_urls.append(specific_img_url)
                                else:
                                    pass
                            elif url.startswith("https://nabu-naturgucker.de"):
                                pass
                            elif url.startswith("https://pixabay.com/"):
                                pass
                            elif url.startswith("https://app.plutof.ut.ee/"):
                                pass
                            elif url.startswith("https://www.flickr.com/"):
                                pass
                            elif ".tif" in url:
                                pass
                            elif url.startswith("https://ib.komisc.ru"):
                                pass
                            elif url.startswith("https://www.facebook.com"):
                                pass
                            elif url.startswith("https://photos.google.com"):
                                pass
                            elif url.startswith("https://animal.depo.msu.ru"):
                                pass
                            elif url.startswith("https://naturgucker.net"):
                                pass
                            elif url.startswith("https://spain.observation.org"):
                                pass
                            elif url.startswith("https://xeno-canto.org"):
                                pass
                            elif url.startswith(
                                "https://fm-digital-assets.fieldmuseum.org"
                            ):
                                pass
                            elif url.startswith("https://www.fossilshells.nl"):
                                pass
                            elif url.startswith(
                                "https://jbrj-public.s3-sa-east-1.amazonaws.co"
                            ):
                                pass
                            elif url.startswith("https://ecos.fws.gov"):
                                pass
                            elif url.startswith("https://id.luomus.fi"):
                                pass
                            elif url.startswith("https://specify.saiab.ac.za"):
                                pass
                            elif (
                                url.startswith(
                                    "https://iiif-manifest.oxalis.br.fgov.be"
                                )
                                and "/manifest" in url
                            ):
                                pass
                            elif (
                                url.startswith("https://iiif.rbge.org.uk")
                                and "/manifest" in url
                            ):
                                pass
                            elif url.startswith("https://www.artportalen.se"):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                img_tag: str = soup.find("img", {"class": "image"})
                                if img_tag:
                                    img_url: str = img_tag["src"]
                                    base_url: str = "https://www.artportalen.se"
                                    absolute_image_url: str = base_url + img_url
                                    change_urls.append(absolute_image_url)
                                else:
                                    pass
                            elif url.startswith("https://www.antweb.org/"):
                                repited_url.append(url)
                            elif url.startswith("https://orthoptera.speciesfile.org"):
                                repited_url.append(url)
                            elif url.startswith("https://www.marinespecies.org/"):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                img_tag: str | None = soup.find(
                                    "img", {"class": "photogallery_thumb"}
                                )
                                if img_tag:
                                    img_url: str = img_tag["src"]
                                    change_urls.append(img_url)
                                else:
                                    pass
                            elif (
                                url.startswith("https://colecciones.uv.es")
                                and not ".jpg" in url
                            ):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                img_tag: str | None = soup.find(
                                    "div", class_="media-render"
                                ).find("img")
                                if img_tag:
                                    img_url: str = img_tag["src"]
                                    change_urls.append(img_url)
                                else:
                                    pass
                            elif url.startswith("https://bins.boldsystems.org"):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                image_elements: list[str] = soup.find_all("img")
                                # Extract the source URLs of the images that start with "//bins.boldsystems"
                                img_tag: str = [
                                    element["src"]
                                    for element in image_elements
                                    if element["src"].startswith("//bins.boldsystems")
                                ]
                                if img_tag:
                                    base_url: str = "https:"
                                    absolute_image_url: str = base_url + img_url
                                    change_urls.append(absolute_image_url)
                                else:
                                    pass
                            elif url.startswith("https://plant.depo.msu.ru/"):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                img_tag: str | None = soup.find(
                                    "img", {"class": "img-responsive"}
                                )
                                if img_tag:
                                    img_url: str = img_tag["src"]
                                    base_url: str = "https://plant.depo.msu.ru/"
                                    absolute_image_url: str = base_url + img_url
                                    change_urls.append(absolute_image_url)
                                else:
                                    pass
                            elif url.startswith(
                                "https://www.inaturalist.org/observations/"
                            ):
                                response: ParseResult = requests.get(url)
                                html_content: str = response.text
                                soup: BeautifulSoup = BeautifulSoup(
                                    html_content, "html.parser"
                                )
                                img_tag: str | None = soup.find(
                                    "meta", property="og:image"
                                )
                                if img_tag:
                                    img_url: str = img_tag["content"]
                                    change_urls.append(img_url)
                                else:
                                    pass
                            else:
                                change_urls.append(url)
                        if repited_url != []:
                            unique_urls: list[str] = set(repited_url)
                            for url in unique_urls:
                                change_urls.append(url)

                        if change_urls == []:
                            obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = None
                        else:
                            obj["INFORMACIÓ COMPLEMENTÀRIA"]["Image_urls"] = change_urls

                file.seek(0)
                json.dump(data, file, indent=4, ensure_ascii=False)
                file.truncate()

            print("Done with images for: " + file_path)
            print()


# --------------------------------------------------------------------------------------------------------------------
# -- DATABASE CONFIGURATION AND POPULATION--- #
# --------------------------------------------------------------------------------------------------------------------


def get_latest_items_from_json_files(directory: str) -> list[dict]:
    """
    Retrieves the latest items from JSON files in a directory.
    Parameters:
        directory: Path to the directory containing JSON files.
    Returns:
        A list of items.
    """

    # List to store items
    items = []

    # Dictionary to store items indexed by base filename
    items_dict = {}

    # Regular expression pattern to extract base filename
    pattern = re.compile(r"(.+)-c\d{4}\.json")

    # Iterate through all files in the directory
    for filename in os.listdir(directory):
        # Check if the file is a JSON file
        if filename.endswith(".json"):
            full_path = os.path.join(directory, filename)
            base_filename_match = pattern.match(filename)
            if base_filename_match:
                base_filename = base_filename_match.group(1)
            else:
                base_filename = os.path.splitext(filename)[
                    0
                ]  # Treat files without dates as unique filenames

            # Read data from the file
            with open(full_path, "r") as f:
                data = json.load(f)

            # Update items_dict based on base filename
            if base_filename in items_dict:
                # If the base filename already exists, update the data only if the file has a later date
                if custom_sort(filename) > custom_sort(
                    items_dict[base_filename]["filename"]
                ):
                    items_dict[base_filename] = {"data": data, "filename": filename}
            else:
                # If the base filename is new, add it to items_dict
                items_dict[base_filename] = {"data": data, "filename": filename}

    # Append the latest version of each base filename to the list of items
    for item_data in items_dict.values():
        items.extend(item_data["data"])

    return items


# Custom sorting function to prioritize files based on their dates
def custom_sort(file_path: str):
    filename = os.path.basename(file_path)
    match = re.search(r"-(c\d{4})", filename)
    if match:
        return match.group(1)
    return ""  # Return empty string if date not found


# --------------------------------------------------------------------------------------------------------------------


def insert_or_update_items(items: dict) -> None:
    """
    Populates the database in mongoDB, checks if exists and update keys.
    Parameter: items dict, of all the json files
    """

    # Connect to the local MongoDB instance
    # client = pymongo.MongoClient(CLIENT)
    # db = client[DATABASE]
    # collection = db[COLLECTION]

    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["Biodiversity"]
    collection = db["species"]

    previous_value = None
    for item in items:
        # Check if the item already exists in the collection
        existing_item = None
        tax_id = item["TAXONOMIA"]["TaxId"]
        especie = item["TAXONOMIA"]["Espècie"]

        # Check in terminal if works well
        current_value: str = item["INFORMACIÓ COMPLEMENTÀRIA"]["Pertany_A"]
        if current_value != previous_value:
            print()
            print(current_value)
            print()
            previous_value = current_value

        print(item["TAXONOMIA"]["Espècie"])

        # If the TaxId key is null, check the Espècie key
        if tax_id:
            existing_item: dict = collection.find_one({"TAXONOMIA.TaxId": tax_id})
        elif tax_id == None:
            existing_item: dict = collection.find_one({"TAXONOMIA.Espècie": especie})

        if not existing_item:
            # If the item doesn't exist, add it to the collection
            collection.insert_one(item)
        else:
            # If the TaxId key is null, check the Espècie key
            if (
                not existing_item.get("TAXONOMIA.TaxId")
                and existing_item.get("TAXONOMIA.Espècie") == especie
            ):
                # If the Espècie key matches, update the existing item with any new keys/values
                update_dict: dict = {}
                for key, value in item.items():
                    if key not in existing_item:
                        update_dict[key] = value
                    elif existing_item[key] != value:
                        update_dict[key] = value
                collection.update_one(
                    {"_id": existing_item["_id"]}, {"$set": update_dict}
                )

            elif tax_id == existing_item["TAXONOMIA"]["TaxId"]:
                # If the Taxid key matches, update the existing item with any new keys/values
                update_dict: dict = {}
                for key, value in item.items():
                    if key not in existing_item:
                        update_dict[key] = value
                    elif existing_item[key] != value:
                        update_dict[key] = value
                collection.update_one(
                    {"_id": existing_item["_id"]}, {"$set": update_dict}
                )

        # Close the connection
        client.close()


# --------------------------------------------------------------------------------------------------------------------


def add_collection_from_json(
    path: str, json_file: str, collection_name: str, key: str
) -> None:
    """
    Add a collection to the database
    Parameters: path where json file is located,
                            json file name,
                            the name of the collection,
                            the key used to check if the object already exists
    """
    # Load JSON data from file
    with open(path + json_file, "r") as file:
        data = json.load(file)

    # Connect to the local MongoDB instance
    # client = pymongo.MongoClient(CLIENT)
    # db = client[DATABASE]
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["Biodiversity"]

    # Check if the collection already exists
    if collection_name in db.list_collection_names():
        collection = db[collection_name]

        # Update the values of existing documents based on the specified key
        if key:
            for obj in data:
                filter_query = {key: obj[key]}
                update_query = {"$set": obj}
                collection.update_one(filter_query, update_query, upsert=True)
    else:
        # Create the collection and insert the documents
        collection = db[collection_name]
        collection.insert_many(data)

    # Close the connection
    client.close()

# --------------------------------------------------------------------------------------------------------------------

def update_descubridor_to_autor():
    """
    Updates the key 'Descubridor' to 'Autor' in the 'IDENTIFICADORS' field in the MongoDB collection.
    """

    # Connect to the local MongoDB instance
    client = pymongo.MongoClient(CLIENT)
    db = client[DATABASE]
    collection = db[COLLECTION]

    # Find all documents that contain the key 'Descubridor' within the 'IDENTIFICADORS' field
    documents = collection.find({"IDENTIFICADORS.Descubridor": {"$exists": True}})

    for document in documents:

        print(document["TAXONOMIA"]["Espècie"])
        # Transfer the value of 'Descubridor' to 'Autor'
        descubridor_value = document["IDENTIFICADORS"]["Descubridor"]
        collection.update_one(
            {"_id": document["_id"]},
            {
                "$set": {"IDENTIFICADORS.Autor": descubridor_value},
                "$unset": {"IDENTIFICADORS.Descubridor": ""},
            },
        )

    # Close the connection
    client.close()

# --------------------------------------------------------------------------------------------------------------------


def update_distribution_key():
    """

    Updates the key 'Distribució' or 'Origen i endemisme' to 'ORIGEN I ENDEMISME' in the MongoDB collection.
    """

    # Connect to the local MongoDB instance
    client = pymongo.MongoClient(CLIENT)
    db = client[DATABASE]
    collection = db[COLLECTION]

    # Find all documents that contain the key 'Distribució' or 'Origen i endemisme'
    documents = collection.find(
        {
            "$or": [
                {"DISTRIBUCIÓ": {"$exists": True}},
                {"Origen i endemisme": {"$exists": True}},
            ]
        }
    )

    for document in documents:
        set_fields = {}
        unset_fields = {}
        print(document["TAXONOMIA"]["Espècie"])

        # If the key 'Distribució' exists in the document, transfer its value
        if "DISTRIBUCIÓ" in document:
            set_fields["ORIGEN I ENDEMISME"] = document["DISTRIBUCIÓ"]
            unset_fields["DISTRIBUCIÓ"] = ""

        # If the key 'Origen i endemisme' exists in the document, transfer its value
        if "Origen i endemisme" in document:
            set_fields["ORIGEN I ENDEMISME"] = document["Origen i endemisme"]
            unset_fields["Origen i endemisme"] = ""

        # Update the document in the collection, removing 'Distribuició' and/or 'Origen i endemisme'
        update_query = {}
        if set_fields:
            update_query["$set"] = set_fields
        if unset_fields:
            update_query["$unset"] = unset_fields

        collection.update_one({"_id": document["_id"]}, update_query)

    # Close the connection
    client.close()


# --------------------------------------------------------------------------------------------------------------------
# --- SUMMARY CREATION FILE --- #
# --------------------------------------------------------------------------------------------------------------------
def sum_num_species(filepath: str, filename: str) -> None:
    """
    With the data in register.json creates a summary file.
    Parameters: filepath str, filename:  str
    """

    # If the file exists read it, if not create a variable
    try:
        with open(filepath + filename, "r") as f:
            data = json.load(f)
    except FileNotFoundError:
        data: list[str] = []

    # Variables to store
    total_num_species: int = 0
    total_taxid_found: int = 0
    total_endemic: int = 0
    total_images_urls: int = 0
    files = files_extractor(JSON_DIRPATH, "json")
    gbif = count_gbifIds_to_species(JSON_DIRPATH, files)
    # Make the pertinent operations like addition
    for item in data:
        if "num_species" in item:
            total_num_species += item["num_species"]

        if "taxId_found" in item:
            total_taxid_found += item["taxId_found"]

        if "count_endemic" in item:
            if type(item["count_endemic"]) == int:
                total_endemic += item["count_endemic"]

        if "valid_images" in item:
            total_images_urls += item["valid_images"]

    if total_num_species == 0 or total_taxid_found == 0:
        percentage_taxid_found: str = "No data found"
    else:
        percentage_taxid_found: int = round(
            (total_taxid_found / total_num_species) * 100, 2
        )

    # If the file exists read it, if not create a variable
    try:
        # Load the existing data from the JSON file
        with open(filepath + "Summary.json", "r") as f:
            summary = json.load(f)
    except FileNotFoundError:
        summary: dict = {}

        # Add the new count data to a "counts" object in the dictionary
    if "Register.json" not in summary:
        summary["Register.json"] = {}

    # Object dictirnary to store
    summary["Register.json"]["total_num_species"] = total_num_species
    summary["Register.json"]["total_taxid_found"] = total_taxid_found
    summary["Register.json"]["percent_taxid_found"] = f"{percentage_taxid_found} %"
    summary["Register.json"]["endemic_species_found"] = total_endemic
    summary["Register.json"]["images_urls_found"] = total_images_urls
    # data Gbif
    summary["Register.json"]["total_GbifId_found"] = gbif[0]
    summary["Register.json"]["total_GbifId_None"] = gbif[1]
    summary["Register.json"]["percentage_with_GbifId"] = f"{gbif[2]} %"

    # Save the dictionary in a json
    with open(filepath + "Summary.json", "w") as f:
        json.dump(summary, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------


def update_tax_id_count_in_json(filepath: str, output_file_path: str) -> dict:
    """
    Make a summary recollection in the summary.json file from DB
    Parameters filepath: string, path to the summary.json file
                       output_file_path: string, name file
    Returns dict of the file to see it in the terminal output
    """

    # Connect to the database
    # client = pymongo.MongoClient(CLIENT)
    # db = client[DATABASE]
    # collection = db[COLLECTION]
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["Biodiversity"]
    collection = db["species"]

    # Count all the documents in the collection
    total_count: int = collection.count_documents({})

    # Count the documents in the collection where the "TaxId" field has an integer value
    tax_id_count: int = collection.count_documents(
        {"TAXONOMIA.TaxId": {"$regex": "^[0-9]+$"}}
    )

    # Count the documents in the collection where in img_url have data not null values
    img_url_count: int = collection.count_documents(
        {"INFORMACIÓ COMPLEMENTÀRIA.Image_urls": {"$ne": None, "$type": "array"}}
    )

    # Count the documents where any of the specified fields have a value that starts with any of the specified prefixes
    endemism_count: int = collection.count_documents(
        {
            "$or": [
                {
                    "ORIGEN I ENDEMISME.Origen i endemisme": {
                        "$regex": "^(Endèmica|Endemisme|Edemisme|Subendemisme)"
                    }
                },
                {
                    "ORIGEN I ENDEMISME.Origen": {
                        "$regex": "^(Endèmica|Endemisme|Edemisme|Subendemisme)"
                    }
                },
                {
                    "ORIGEN I ENDEMISME.Endemisme": {
                        "$regex": "^(Endèmica|Endemisme|Edemisme|Subendemisme)"
                    }
                },
                {
                    "ORIGEN I ENDEMISME.Estatus a Catalunya": {
                        "$regex": "^(Endèmica|Endemisme|Edemisme|Subendemisme)"
                    }
                },
            ]
        }
    )

    gbif_count: int = collection.count_documents(
        {
            "INFORMACIÓ COMPLEMENTÀRIA.GbifId": {
                "$ne": None,
            }
        }
    )

    # If the file exists read it, if not create a variable
    try:
        # Load the existing data from the JSON file
        with open(filepath + output_file_path, "r") as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {}

        # Add the new count data to a "counts" object in the dictionary
    if "Database" not in data:
        data["Database"] = {}

    data["Database"]["Count_species"] = total_count
    data["Database"]["TaxId_count"] = tax_id_count
    data["Database"][
        "TaxId_percentage"
    ] = f"{round((tax_id_count/total_count) * 100, 2)} %"
    data["Database"]["GbifId_count"] = gbif_count
    data["Database"][
        "GbifId_percentage"
    ] = f"{round((gbif_count/total_count) * 100, 2)} %"
    data["Database"]["Endemics_count"] = endemism_count
    data["Database"]["stored_img"] = img_url_count

    # Write the updated data back to the file
    with open(filepath + output_file_path, "w") as f:
        json.dump(data, f, indent=4, ensure_ascii=False)

    return data


# --------------------------------------------------------------------------------------------------------------------
# --- FOOTERS --- #
# --------------------------------------------------------------------------------------------------------------------


def remove_nones(lista):
    """Remove all empty elements from a list"""
    return [
        elemento for elemento in lista if (elemento is not None and len(elemento) > 0)
    ]


# --------------------------------------------------------------------------------------------------------------------
# DIPTERS QUIRONOMIDS
def get_ref_data(refs, footer_list):
    """Get the footer information from the given reference list"""
    info_list = []
    for ref in refs:
        for i in range(len(footer_list)):
            if ref in footer_list[i][:4]:
                info_list.append(footer_list[i].replace(ref, "").strip())
    return info_list if len(info_list) > 0 else None


# --------------------------------------------------------------------------------------------------------------------


def convert_array_to_dict(arr):
    """Convert an array to dict FOOTER information
    #!(ONLY FOR DIPTERS_QUIRONOMIDS)
    """
    result_dict = {}
    for item in arr[1:]:
        key, value = item.split(" = ")
        result_dict[key] = value

    return result_dict


# --------------------------------------------------------------------------------------------------------------------


def replace_elements(lst, dct):
    new_lst = []
    for item in lst:
        multiple_keys = item.split(", ")
        replaced_elements = [dct.get(key, key) for key in multiple_keys]
        new_lst.append(", ".join(replaced_elements))
    return new_lst


# --------------------------------------------------------------------------------------------------------------------
# ----- GBIF FUNCTIONS ----#
# --------------------------------------------------------------------------------------------------------------------
def get_kingdom_of_GBIF(species_name):
    """
    Get Kingdom of GBIF for a given species
    """

    # Retrieve record for the specie name
    try:
        # Search for occurrences on GBIF API
        search_url = (
            f"https://api.gbif.org/v1/occurrence/search?scientificName={species_name}"
        )
        response = requests.get(search_url)
        occurrence_data = response.json()

        # Retrieve kingdom of specie
        if "results" in occurrence_data and occurrence_data["results"]:
            kingdom: str = occurrence_data["results"][0]["kingdom"]
        else:
            kingdom: str = None
        return kingdom

    except requests.exceptions.RequestException as e:
        print("An error occurred while accessing GBIF API:", str(e))

    return None


# --------------------------------------------------------------------------------------------------------------------
def get_gbifId_of_GBIF(specie_name):
    """
    Get gbifId of GBIF for a given species
    """

    # Retrieve record for the specie name
    try:
        # Search for occurrences on GBIF API
        search_url = (
            f"https://api.gbif.org/v1/occurrence/search?scientificName={specie_name}"
        )
        response = requests.get(search_url)
        occurrence_data = response.json()

        # Retrieve gbifID of specie
        if "results" in occurrence_data and occurrence_data["results"]:
            gbifID = occurrence_data["results"][0]["gbifID"]
        else:
            gbifID = None

        return gbifID

    except requests.exceptions.RequestException as e:
        print("An error occurred while accessing GBIF API:", str(e))

    return None


# --------------------------------------------------------------------------------------------------------------------
def add_gbifID_to_species(path: str, json_files: list[str]) -> None:
    """
    Add the GbifId to the documents
    Retrieves the gbifId and adds it to each species in the json.
    """

    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"GbifId to {file}")
        for spe in objects:
            info_plus = spe.get("INFORMACIÓ COMPLEMENTÀRIA", {})
            gbifdata = info_plus.get("GbifId", {})

            gbifId: str = None
            specie: str = None

            if gbifdata and gbifdata != None:
                # If GbifId is present then continue
                continue
            else:
                specie: str = spe["TAXONOMIA"]["Espècie"]
                # print(f'Especie sin GbifID en sus claves  {specie}')
                gbifId = get_gbifId_of_GBIF(specie)

            if gbifId:
                spe["INFORMACIÓ COMPLEMENTÀRIA"]["GbifId"] = gbifId
            else:
                spe["INFORMACIÓ COMPLEMENTÀRIA"]["GbifId"] = None

        # Sort the keys within each object
        sorted_data: list[dict] = []
        for obj in objects:
            sorted_obj: dict = {key: obj[key] for key in sorted(obj.keys())}
            sorted_data.append(sorted_obj)

        # Write the updated JSON file
        with open(f"{path}/{file}", "w") as f:
            json.dump(sorted_data, f, indent=4, ensure_ascii=False)


# --------------------------------------------------------------------------------------------------------------------
def count_gbifIds_to_species(
    path: str, json_files: list[str]
) -> tuple[int, int, float]:
    """
    Counts the number of gbifId the species
    """
    total_species = 0
    total_GbifId_found: int = 0
    total_GbifId_None: int = 0

    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)
        print(file)
        # print(f'Kingdom to {file}')
        total_species += len(objects)
        for spe in objects:
            if "INFORMACIÓ COMPLEMENTÀRIA" in spe.keys():  # and spe["Espècie"] != None
                # If GbifId is present and not None, increment total_GbifId count
                if spe["INFORMACIÓ COMPLEMENTÀRIA"]["GbifId"] != None:
                    total_GbifId_found += 1
                # If GbifId is present but None, increment count_GbifId_None count
                else:
                    total_GbifId_None += 1
            else:
                pass

    # Calculate percentages
    percentage_with_GbifId = round((total_GbifId_found / total_species) * 100, 2)

    return total_GbifId_found, total_GbifId_None, percentage_with_GbifId


# --------------------------------------------------------------------------------------------------------------------


def add_kingdom_gbif_to_species(path: str, json_files: list[str]) -> None:
    """
    Search Gbif for the realm of the species they do not yet have.
    Retrieve the realm, reassign the realm name and add it to the species in the json.
    """

    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"Regne to {file}")
        for spe in objects:

            kingdom: str = None
            specie: str = None

            # Retrive the Kingdom of every specie
            if "Regne" in spe.keys() and spe["Regne"] != None:
                # If kingdom is present then continue
                continue
            else:
                specie: str = spe["Espècie"]
                kingdom: str = get_kingdom_of_GBIF(specie)
                print(f"Process Species: {specie}")

            # Reassign species name
            # NCBI: Metazoa, Viridiplantae, Fungi
            # Gbif: Animalia, Plantae, Fungi, Chromista, Protozoa, Bacteria

            # Diccionario para mapear los valores de "kingdom" a los valores de "Espècie"
            kingdom_to_species = {
                "Animalia": "Metazoa",
                "Plantae": "Viridiplantae",
                "Fungi": "Fungi",
                "Protozoa": "Protist",
                "Chromista": "Protist",
                "Bacteria": "Bacteria",
            }

            if kingdom:
                # Obtener el valor correspondiente de "Espècie" del diccionario o None si no se encuentra
                spe["Regne"] = kingdom_to_species.get(kingdom, None)

            else:
                # Asignar None a la clave "Espècie" si "kingdom" es None o no está definido
                spe["Regne"] = None

        # Sort the keys within each object
        sorted_data: list[dict] = []
        for obj in objects:
            sorted_obj: dict = {key: obj[key] for key in sorted(obj.keys())}
            sorted_data.append(sorted_obj)

        # Write the updated JSON file
        with open(f"{path}/{file}", "w") as f:
            json.dump(sorted_data, f, indent=4, ensure_ascii=False)

    return


# --------------------------------------------------------------------------------------------------------------------
def get_uicn_of_GBIF(specie_name) -> None | str:
    """
    Get UICN of GBIF for a given species
    """

    try:
        # Search for occurrences on GBIF API
        search_url = (
            f"https://api.gbif.org/v1/occurrence/search?scientificName={specie_name}"
        )
        response = requests.get(search_url)
        occurrence_data = response.json()

        # Retrieve kingdom of specie
        if (
            "results" in occurrence_data
            and occurrence_data["results"]
            and "iucnRedListCategory" in occurrence_data["results"][0]
        ):
            uicn: str = occurrence_data["results"][0]["iucnRedListCategory"]
        else:
            uicn: str = None
        return uicn

    except requests.exceptions.RequestException as e:
        print("An error occurred while accessing GBIF API:", str(e))

    return None


# ----------------------------------------------------------------
def add_uicn_gbif_to_species(path: str, json_files: list[str]) -> None:
    """
    Search Gbif for 'IUCN' for species that don't have it yet.
    Verify that the key SITUACIÓ exists and add the 'IUCN GBIF' key with its value,
    if it does not exist, create it and add 'IUCN GBIF'
    """
    for file in json_files:
        # Load the JSON file
        with open(f"{path}/{file}", "r") as f:
            objects = json.load(f)

        print(f"UICN to {file}")
        for spe in objects:
            specie = spe["TAXONOMIA"]["Espècie"]
            uicn = get_uicn_of_GBIF(specie)
            print(f"Process Species: {specie}")

            if "SITUACIÓ" not in spe:
                spe["SITUACIÓ"] = {}

            spe["SITUACIÓ"]["UICN GBIF"] = uicn

        # Sort the keys within each object
        sorted_data = [{key: obj[key] for key in sorted(obj.keys())} for obj in objects]

        # Write the updated JSON file
        with open(f"{path}/{file}", "w") as f:
            json.dump(sorted_data, f, indent=4, ensure_ascii=False)

    return


# --------------------------------------------------------------------------------------------------------------------
# ----- MODIFY STRUCTURE JSONS, STANDARD KEYS ----#
# --------------------------------------------------------------------------------------------------------------------


def replace_key_in_json_files(folder_path: str, old_key: str, new_key: str) -> None:
    """
    Change the keys of JSON objects to new ones.

    Parameters:
    - folder_path: str, path to the folder containing the JSON files.
    - old_key: str, the key to be replaced.
    - new_key: str, the new key to replace the old key with.
    """
    # Get a list of all JSON files in the folder
    json_files = [file for file in os.listdir(folder_path) if file.endswith(".json")]

    for json_file in json_files:
        file_path = os.path.join(folder_path, json_file)

        with open(file_path, "r+", encoding="utf-8") as file:
            try:
                data = json.load(file)
                print(f"Processing file: {file_path}")

                def replace_key(obj):
                    """Replace key in JSON objects recursively."""
                    if isinstance(obj, dict):
                        for key in list(obj.keys()):
                            if key == old_key:
                                obj[new_key] = obj.pop(old_key)
                            else:
                                replace_key(obj[key])
                    elif isinstance(obj, list):
                        for item in obj:
                            replace_key(item)

                replace_key(data)

                # Go back to the beginning of the file and overwrite the content
                file.seek(0)
                json.dump(data, file, indent=4, ensure_ascii=False)
                file.truncate()
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON from file {file_path}: {e}")
            except Exception as e:
                print(f"An error occurred while processing file {file_path}: {e}")


# --------------------------------------------------------------------------------------------------------------------


def group_and_sort_keys_in_json_files(
    folder_path: str, keys_to_group: list[str], new_group_key: str
) -> None:
    """
    Group keys in a json to better comprehension
    Parameters: folder_path where are all the json files
                            keys to group, list of keys to insert into a group
                            new_group_key name of the group

    """
    # Get a list of all JSON files in the folder
    for json_file in os.listdir(folder_path):
        if json_file.endswith(".json"):
            file_path: str = os.path.join(folder_path, json_file)

            with open(file_path, "r+") as file:
                data: list[dict] = json.load(file)

                # Check if any key from keys_to_group exists in the JSON objects
                if any(key in obj for obj in data for key in keys_to_group):
                    for obj in data:
                        # Check if the new_group_key already exists in the object
                        if new_group_key in obj:
                            group: dict = obj[new_group_key]
                        else:
                            # If new_group_key does not exist, create it as an empty dictionary
                            group: dict = {}
                            obj[new_group_key] = group

                        # Add keys to the group if they exist in the object
                        for key in keys_to_group:
                            if key in obj:
                                group[key] = obj[key]
                                del obj[key]

                # Go back to the beginning of the file and overwrite the content
                file.seek(0)
                json.dump(
                    data, file, indent=4, sort_keys=True, ensure_ascii=False
                )  # Sort keys alphabetically
                file.truncate()

# --------------------------------------------------------------------------------------------------------------------

def order_data():
    """
    Order the json data to match the database data scheme
    """

    # All the headers grouped
    taxon_group: list[str] = [
        "Regne",
        "Fílum",
        "Phylum",
        "Classe",
        "Superordre",
        "Ordre",
        "Subordre",
        "Superfamília",
        "Família",
        "Familia",
        "Subfamília",
        "Tribu",
        "Espècie",
        "Nom comú",
        "TaxId",
        "Divisió",
        "Grup",
        "Supergrup",
        "Subclasse",
        "Infraclasse",
    ]
    legislacio_group: list[str] = [
        "Reial Decret 630/2013",
        "Reial Decret 139/2011",
        "Directiva Hàbitats 92/43/CEE",
        "Directiva hàbitats",
        "Llei 2/2008",
        "Decret 328/1992 PEIN",
        "Conveni per a la protecció de les espècies migradores d'animals silvestres (CMS)",
        "Invertebrats que requereixen mesures de conservació a Catalunya (ICHN, 2008)",
        "Invertebrats que requereixen mesures de conservació a Catalunya (ICHN  2008; 2010)",
        "Fongs, líquens i briòfits que requereixen mesures de protecció a Catalunya (ICHN, 2010)",
        "Conveni comerç internacional (CITES)",
        "Catàleg de flora amenaçada",
        "Catàleg de fauna amenaçada (proposta)",
        "Catàleg de fauna amenaçada (proposta 2019)",
        "Catàleg de fauna amenaçada (proposta 2020)",
        "Catàleg de fauna amenaçada (Decret 172/2022)",
        "Directiva Ocells",
        "Protocol sobre les Àrees Especialment Protegides i la diversitat biològica a la Mediterrània (SPA/BD)",
        "Protocol relatiu a la diversitat biològica a la Mediterrània",
        "Protocol relatiu a la diversitat biològica a la Mediterrània - Reial Decret 139/2011",
        "Llistat d'espècies pesqueres i d'aqüicultura (BOE, 2019)",
        "Conveni sobre la conservació de la vida silvestre i del medi natural d'Europa",
        "Reglament (CE) 1967/2006. Talles mínimes (cm) per a l'explotació sostenible dels recursos pesquers a la Mediterrània",
        "Reglament (CE) 520/2007. Mesures tècniques de conservació de poblacions de peixos altament migratoris",
        "Reglament (UE) 2015/104. Possibilitats de pesca per a determinades poblacions de peixos",
        "Reglament (UE) 2015/ 2102. Disposicions aplicables a la pesca a la Mediterrània",
        "Reglament d'Execució (UE) 2016/1141",
        "Ordre AAA/658/2014. Regulació de la pesca amb palangre de super-fície per  a espècies altament migratòries",
        "Llibre Vermell dels Ortòpters Espanyols (1985)",
        "Llista Vermella Europea de llagosta, grills i grills de matoll (2016)",
        "Llista Vermella (ICO, 2013)",
        "Llista Vermella preliminar de fongs a protegir de la peninsula Iberica",
        "Llibre vermell Espanya (2011)",
        "Atles i Llibre Vermell dels Invertebrats Amenaçats d'Espanya (2011)",
        "Atles dels invertebrats amenaçats d'Espanya (2011)",
    ]
    distr_group: list[str] = [
        "Origen i endemisme",
        "Origen",
        "Endemisme",
        "Estatus a Catalunya",
        "Origen / Entrada",
    ]
    situacio_group: list[str] = [
        "UICN (Europa)",
        "UICN (Mediterrani)",
        "UICN (Catalunya) estimada",
        "UICN (Regional)",
        "UICN (Espanya)",
        "UICN (Global)",
        "UICN (Catalunya) (proposta)",
        "UICN",
        "UICN (Catalunya)",
        "UICN GBIF",
        "Actuacions",
        "Amenaça",
        "Conservació",
        "Estatus (Olmo-Vidal, 2002, 2006)",
        "Grau d'amenaça",
        "Índex de Vulnerabilitat dels Odonats de Catalunya",
        "Localització",
        "Presència",
        "Protecció",
        "Estatus",
        "Actuacions sobre el terreny",
    ]
    estudis_group: list[str] = [
        "Estudis moleculars",
        "Genoma",
        "Estudis moleculars (Genbank)",
        "Estudis genètico-moleculars",
        "Estudis genètics i moleculars",
    ]
    iden_group: list[str] = [
        "Autor",
        "Referència a WORMS",
        "Referència bibliogràfica www.aula2005.com/html/anfipodos",
        "Autor i publicació",
    ]
    carac_group: list[str] = [
        "Estadi",
        "Macrolíquen",
        "Fong liquenificat, o no, que viu sobre un líquen",
        "Perillositat",
        "Hoste",
        "Fong no liquenificat habitualment estudiat pels liquenòlegs",
        "Microlíquen",
    ]
    eco_group: list[str] = [
        "Ambient",
        "Hàbitat",
        "Columna d'aigua",
        "Distància a la costa",
    ]
    infoplus_group: list[str] = [
        "Vies d'entrada",
        "Llistats on s'inclou, o no, l'espècie",
        "Interès comercial",
        "Altres consideracions",
        "Images_url",
        "Pertany_A",
        "GbifId",
        "Observacions",
    ]

    # Changing the order of the data
    group_and_sort_keys_in_json_files(JSON_DIRPATH, taxon_group, "TAXONOMIA")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, legislacio_group, "LEGISLACIÓ")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, distr_group, "ORIGEN I ENDEMISME")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, situacio_group, "SITUACIÓ")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, estudis_group, "ESTUDIS")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, iden_group, "IDENTIFICADORS")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, carac_group, "CARACTERÍSTIQUES")
    group_and_sort_keys_in_json_files(JSON_DIRPATH, eco_group, "ECOLOGIA")
    group_and_sort_keys_in_json_files(
        JSON_DIRPATH, infoplus_group, "INFORMACIÓ COMPLEMENTÀRIA"
    )

# --------------------------------------------------------------------------------------------------------------------

def overwrite_regne(source_folder: str, destination_folder: str) -> None:
    """
    Overwrite the key Regne given 2 folders. To merge versions
    Parameters: source_folder where the values are stored
                            destination folder where the values will be overwritten
    """
    # Get a list of all the JSON files in the source folder
    source_json_files: list[str] = [
        f for f in os.listdir(source_folder) if f.endswith(".json")
    ]

    for filename in source_json_files:
        source_file_path: str = os.path.join(source_folder, filename)
        with open(source_file_path, "r") as source_file:
            source_data: list[dict] = json.load(source_file)

        source_dict: dict = {
            obj.get("Espècie"): obj.get("Regne") for obj in source_data
        }

        # Get the corresponding destination file
        destination_file_path: str = os.path.join(destination_folder, filename)

        with open(destination_file_path, "r") as destination_file:
            destination_data: list[dict] = json.load(destination_file)

        # Overwrite the "Regne" value in all objects of the destination file
        if destination_data and isinstance(destination_data, list):
            for obj in destination_data:
                if "TAXONOMIA" in obj:
                    obj_id: str = obj["TAXONOMIA"].get("Espècie")
                    if obj_id and obj_id in source_dict:
                        obj["TAXONOMIA"]["Regne"] = source_dict[obj_id]

        # Write the modified data back to the destination file
        with open(destination_file_path, "w") as destination_file:
            json.dump(destination_data, destination_file, indent=4, ensure_ascii=False)

# --------------------------------------------------------------------------------------------------------------------

def update_json_files(folder_path, excel_data):
    """
    Updates JSON files in a folder based on species or TaxId found in excel_data.

    Args:
        folder_path (str): Path to the folder containing JSON files.
        excel_data (list): List of dictionaries from the Excel JSON.
    """
    for filename in os.listdir(folder_path):
        if filename.endswith(".json"):
            file_path = os.path.join(folder_path, filename)

            # Load JSON
            with open(file_path, 'r', encoding='utf-8') as json_file:
                try:
                    json_data = json.load(json_file)
                    if not isinstance(json_data, list):
                        print(f"⚠ Warning: {filename} does not contain a list. Skipping.")
                        continue
                except json.JSONDecodeError:
                    print(f"❌ Error: Failed to decode {filename}. Skipping.")
                    continue

            updated = False

            # Iterate in the JSON list
            for obj in json_data:
                taxon_data = obj.get("TAXONOMIA", {})

                for excel_entry in excel_data:
                    taxid = excel_entry.get("TaxId")
                    especie = excel_entry.get("Espècie")

                    # If TaxId is "N/D", treat it as null
                    if taxid == "N/D":
                        taxid = None

                    # Match using TaxId or Espècie
                    if (taxid and taxid == taxon_data.get("TaxId")) or (especie and especie == taxon_data.get("Espècie")):
                        print(f"🔄 Updating {filename} for species: {especie}")

                        regne = excel_entry.get("Regne")
                        if regne == "N/D":
                            regne = None
                        filum = excel_entry.get("Fílum")
                        if filum == "N/D":
                            filum = None
                        classe = excel_entry.get("Classe")
                        if classe == "N/D":
                            classe = None
                        ordre = excel_entry.get("Ordre")
                        if ordre == "N/D":
                            ordre = None
                        familia = excel_entry.get("Família")
                        if familia == "N/D":
                            familia = None
                        especie = excel_entry.get("Espècie")
                        if especie == "N/D":
                            especie = None

                        taxid = excel_entry.get("TaxId")
                        if taxid == "N/D":
                            taxid = None

                        # Update TAXONOMIA fields
                        taxon_data.update({
                            "Regne": regne,
                            "Fílum": filum,
                            "Classe": classe,
                            "Ordre": ordre,
                            "Família": familia,
                            "Espècie": especie,
                            "TaxId": taxid
                        })
                        obj["TAXONOMIA"] = taxon_data 

                        # Update SITUACIÓ fields
                        uicn_gbif = excel_entry.get("UICN GBIF")
                        if uicn_gbif == "N/D":
                            uicn_gbif = None
                        obj.setdefault("SITUACIÓ", {})["UICN GBIF"] = uicn_gbif

                        # Update INFORMACIÓ COMPLEMENTÀRIA fields
                        gbifid = excel_entry.get("GbifId")
                        if gbifid == "N/D":
                            gbifid = None
                        obj.setdefault("INFORMACIÓ COMPLEMENTÀRIA", {})["GbifId"] = gbifid

                        # Update IDENTIFICADORS fields
                        autor = excel_entry.get("Autor")
                        if autor == "N/D":
                            autor = None
                        obj.setdefault("IDENTIFICADORS", {})["Autor"] = autor

                        updated = True

            # Save changes
            if updated:
                with open(file_path, 'w', encoding='utf-8') as json_file:
                    json.dump(json_data, json_file, ensure_ascii=False, indent=4)
                print(f"✅ Updated file: {filename}")

# --------------------------------------------------------------------------------------------------------------------

def move_keys_to_info_complementaria(folder_path):
    """
    Moves specific keys from 'NOM DE L'AUTOR' to 'INFORMACIÓ COMPLEMENTÀRIA'
    in all JSON files within a given folder.

    Args:
        folder_path (str): Path to the folder containing JSON files.
    """
    keys_to_move = [
        "Referència a WORMS",
        "Referència bibliogràfica www.aula2005.com/html/anfipodos",
        "Autor i publicació",
    ]

    for filename in os.listdir(folder_path):
        if filename.endswith(".json"):
            file_path = os.path.join(folder_path, filename)

            # Load JSON
            with open(file_path, 'r', encoding='utf-8') as json_file:
                try:
                    json_data = json.load(json_file)
                    if not isinstance(json_data, list):  
                        print(f"⚠ Warning: {filename} does not contain a list. Skipping.")
                        continue
                except json.JSONDecodeError:
                    print(f"❌ Error: Failed to decode {filename}. Skipping.")
                    continue

            updated = False

            for obj in json_data:
                nom_autor_data = obj.get("NOM DE L'AUTOR", {})
                info_complementaria = obj.setdefault("INFORMACIÓ COMPLEMENTÀRIA", {})

                # Move only the specified keys
                for key in keys_to_move:
                    if key in nom_autor_data:
                        if key not in info_complementaria:
                            info_complementaria[key] = nom_autor_data.pop(key)
                            updated = True

            # Save changes back if updated
            if updated:
                with open(file_path, 'w', encoding='utf-8') as json_file:
                    json.dump(json_data, json_file, ensure_ascii=False, indent=4)
                print(f"✅ Updated file: {filename}")



