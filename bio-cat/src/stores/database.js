import { defineStore } from "pinia";
import api from "../composables/axios-config.js"

export const useDatabaseStore = defineStore("databaseStore", {
  state: () => ({
    allSpecies: [],
    specieData: null,
    countSpecie: null,
    allKingdoms: null,
    classes: [],
    families: [],
    divisions: [],
    orders: [],
    speciesSuggestions: [],
    phylotree: [],
    wikipediaData: [],
    allFamilies: [],
    statisticalData: null,
    name: 'DatabaseStore'
  }),
  actions: {
    async fetchAllSpecies() {
      try {
        const response = await api.get('/Biodb/all');
        this.allSpecies = response.data;
      } catch (err) {
        console.error('Error fetching allSpecies:', err);
      }
    },
    async fetchKingdoms() {
      try {
        const response = await api.get('/Biodb/kingdoms')
        this.allKingdoms = response.data;
      } catch (err) {
        console.error('Error fetching Kingdoms:', err);
      }
    },
    async fetchClasses(kingdoms) {
      try {
        const response = await api.get('/Biodb/classes', {
          params: { kingdoms },
        });
        this.classes = response.data;
      } catch (err) {
        console.error('Error fetching classes:', err);
      }
    },
    async fetchFamilies(kingdoms, classes) {
      try {
        const params = {};

        if (kingdoms) {
          params.kingdoms = kingdoms;
        }

        if (classes) {
          params.classes = classes;
        }

        const response = await api.get('/Biodb/families', {
          params,
        });
        this.families = response.data;
      } catch (err) {
        console.error('Error fetching families:', err);
      }
    },
    async fetchDivision(divisions) {
      try {
        const response = await api.get('/Biodb/divisions')
        this.divisions = response.data;
      } catch (err) {
        console.error('Error fetching division:', err);
      }
    },
    async fetchOrder(order) {
      try {
        const response = await api.get('/Biodb/order')
        this.orders = response.data;
      } catch (err) {
        console.error('Error fetching division:', err);
      }
    },
    async fetchSpecie(specie) {
      try {
        const response = await api.get(`/Biodb/specie/${specie}`);
        this.specieData = response.data;
      } catch (err) {
        console.error('Error fetching specieData:', err);
      }
    },
    async fetchSpecieCount() {
      try {
        const response = await api.get('/Biodb/count')
        this.countSpecie = response.data.count
      } catch (err) {
        console.error('Error fetching countSpecie:', err)
      }
    },
    async fetchSpeciesSuggestions(query) {
      try {
        const response = await api.get(`/Biodb/suggestions/${query}`);
        this.speciesSuggestions = response.data;
      } catch (err) {
        console.error('Error fetching Suggestions:', err);
      }
    },
    async fetchPhylotree(classe, families) {
      try {
        const params = {};
        if (classe){
          params.classe = classe
        }

        if (families !== undefined) {
          params.families = families
        }

        const response = await api.get('/Biodb/phylotree', {params});
        this.phylotree = response.data;

      } catch (err) {
        console.error('Error fetching Phylotree:', err);
      }
    },
    async fetchWikipediaData(specieName, language) {
      try {
        const response = await api.get(`/Biodb/WikipediaData/${specieName}/${language}`);
        this.wikipediaData = response.data;
      } catch (err) {
        console.error('Error fetch Wikipedia Data:', err);

      }
    },
    async fetchAllFamilies(classe) {
      try {
        const params = {};
        if (classe){
          params.classe = classe
        }
        const response = await api.get('/Biodb/allFamilies', {params});
        this.allFamilies = response.data;

      } catch (err) {
        console.error('Error fetching all Families:', err);
      }
    },
    async fetchStatisticalData() {
      try {
        const response = await api.get('/Biodb/statisticalData');
        this.statisticalData = response.data.statisticalData
      } catch (err) {
        console.error('Error fetching statisticalData:', err)
      }
    },
  }
});