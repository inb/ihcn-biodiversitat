# IHCN-Biodiversitat

A project with researchers in Catalonia to catalogue its biodiversity and make a web application.

---
---
## BACKEND

All the scripts and functions that involve the cleaning and uploading the data to the database.

---
---
## <u>INDEX</u>

INTRODUCTION:
- [Files involved](#listfiles)
    - [Files](#listscripts)
    - [Directories](#listdirectories)

THE CODE :
- [Description](#descripcion)
- [Before Starting](#before-start)
- [Libraries](#liblist)
- [Steps](#steps)

---
---

## <a id="listfiles">Files of this repository</a> 

### <a id="listscripts">_Files_</a> 

- [main.py](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/main.py) -> Main script to execute

- [utils.py](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/utils.py) -> Group of functions to use in main.py

- [check_online.py](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/API_Google/check_online.py) -> Script to check if the remote directory is updated with the local version and sent the response in the mail once a week.
  
- [Footers.json](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/Results/Footers.json) -> Recopilation of all the data in the footers of every file.
  
- [Register.json](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/Results/Register.json) -> Recopilation of data of every excel.

- [Summary.json](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/Results/Summary.json) -> Final counter of all excels.
  
- [requirements.txt](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/Configuration/requirements.txt) -> File with all the requirements to a conda enviroment ready to install to use this scripts. * 
  
  -  *`conda create --name <name> --file requirements.txt`
  
- [mail_remainder_script.sh](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/mail_remainder_script.sh) -> Script that executes with cron configuration a weekly email notification.

- [config.ini](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/Backend/Configuration/config.ini) -> File with all the routes and configuration that is used in the main and utils.

###  <a id="listdirectories">_Directories_</a>
 
- [API_Google](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/API_Google) -> Where all files related to API_Google are stored.

  - [google_api_config](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/API_Google/google_api_config) -> Files that need the google api to connect to google drive service
  
- [Configuration](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Configuration) -> Where all configuration files are stored, including configuration and requirements.
  
- [Files](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files) -> Saves all the files in excel, json, html
   
  - [excels-batch](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/excels-batch) -> Batch of files to make a conversion and run the script to avoid bugs.
  
  - [excels](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/excels) -> Recopilated files to work with, mantain the original version
  
  - [json](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/json) -> Saves all the json files created
  
  - [modified_files](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/modified_files) -> Where all files that had been modified are saved.
  
    - [doc](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/modified_files/doc) -> Where step one of getting only species is saved. 
  
    - [tmp](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/modified_files/tmp) -> Where step two of getting only species is saved.
  
    - [xlsx_mofified](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/modified_files/xlsx_modified) -> All excels that in excels directory but modified

- [Results](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Results) -> Saves all the results files, registers, footers, summary.

<br/>

# THE CODE: Cleaning Datasource

## <a id="descripcion">Description</a> 
This part of the project contains the manipulation of the data to set the database. Scripts to normalize all the data and split some information in two colums to further investigation.

---
## <a id="before-start">Before starting</a> 
To split the column species with authors in two different columns each. It has been used a manual method, which consist to copy only the species column to an .odt file and then assure all the species are in italic format.

When it is done reviwing this, tranform this file into .html format. Once done, this file will  be the data to execute the script to do a webscraping only to get a list of species.

When gets the list of only species, we can create a json with the excel data and the column specie splited. In addition, to expand the data we addon  the tax id of every specie if found from ncbi taxonomy database, to in the future, if we want to adden up more information we could use the tax id to extract it.

Furthermore it is created a log file that count the number of species that there are in each file and the number of authors not duplicated, adding too the number of tax id found.

It has been created a final log file with all the information recopilated from the individual log files making it a resume of both files.


## <a id="liblist">Used libraries</a> 
- Biopython, Entrez
- Openpyxl, Beautifulsoup
- Json, os, io, pickle, Path, configparser, time
- googleapiclient, google_auth_oauthlib
  

---

## <a id="steps">*STEPS:*</a> 

### __STEP ONE:__  Check for data updates.

> Once a week the script `main_remainder_script.sh` excutes the script `check_online.py` to check if there is a file in the google drive modified, erased or added that is not in the local file.
> 
> If there is new files or modified ones do the step below.
>
> The mails that are used are in the configuration file, this script uses .env file with local and personal variables, before execute it make sure that this file exists in the same level as the script. 
>
> Check this variables -> $CONDA_ENV_PATH and $CONDA_ENV_NAME that are in the script .sh and EMAIL_PASSWORD that is in check_online.py

### __STEP ONE HALF:__  Get only the species columns

__* This step only if is required, not do it all the times!*__

> It's time to create manually files with only the species columns.
> - From __google sheet__ copy the column specie without headers and footers to doc or odt file whose name is the name of the excel file.
> - In the doc or odt file make sure that only the specie is in italic format (some files are not in that way).
>    - If it may be modified, for faster edition could use ->
>        - edit -> find and replace (ctrl + h) ->  Mark regular expresion checkbox -> in find input insert regex -> find all -> in docs menu select italic or (ctrl + i)
>        - regex expressions could match *reviewing before change*, `^\w+\s+\w+` (to mathch species with 2 names) or `^\w+\s+\(\w+\)+\s+\w+` (to match specie (specie) specie)  _*could be more cases_
>        - Sometimes the column is ok except the word 'var.', 'cf.', 'f.', 'gr.' that is not in italic. To transform it quickly just select one, ctrl + f, find all and then tranforming in italic, by the menu or ctrl + i.
> - Save the file, and then covert it to html format. To do so:
>   - Could be done many files at the same time:
>       - In the terminal once done saving the odt write and in the folder where are the odt `libreoffice --convert-to html --outdir where/you/have/located/files/modified_files/tmp/ *.odt` _*write the complete path_ 
>       - (to avoid warnings of ovwerwriting in terminal output mantain an odt file open)
> 
> Check the html output in the browser, and the once the page is open mouse right button -> view page source
>   - Sometimes the files with species like specie (specie) specie it is not tranformated well, do a quick check. If the parenthesis are in 'span' tags and not in 'i' tags then:
>       - With the last regex expression above get all the matches, clear direct formating button or ctrl + m and then ctrl + i or italic button.
>       - Once done save again the odt in html with the terminal command again and check again the html in the browser. Now all the specie must be between 'i' tags.


### __STEP TWO:__  Execute the script

> To execute the `file_parser()` function that gets the data from excel to json in the main, the excel has to be in specific way:
> - Make sure that row 3 has no merge rows. The data of the row 3 will be used as header. If needs modification save the file as excel 2007-365 format(.xlsl) and in the directory [xlsx_modified](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend/Files/modified_files/xlsx_modified)
> 
> The `main.py` script have multiple functions, extract data from the original excels to jsons, Split the column specie if had discoverer in it and add TaxId, Class and Kingdom to expand the information of each specie. Also if a image is founded in a public library is added to the specie object.
>
> Then if it is necessary modify the keys or data in the json if requiered. There is a function that review the keys to check if there are double and add kingdoms, class, img, etc.
>
> In the final step adds the data of the json in a mongodb database in a container and creates a registry, summary to made a check up of the information of all the data.
>
> The description of this database will be descrived in the data folder.

