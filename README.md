# IHCN-Biodiversitat

A project with researchers in Catalonia to catalogue its biodiversity and make a web application.

---
---

<br />

## <u>INDEX</u>

INTRODUCTION:
- [Description of the Project](#descripcion)
- [Repository distribution](#listfiles)
    - [Folders](#folders)
    - [Files](#files)

ABOUT :

- [Before Starting](#before-start)
- [Tools](#liblist)
- [Steps](#steps)
- [Present](#nowdays)

<br />

---
---
<br />

## <a id="descripcion">What is this catalogue?</a>
The aim of this project is to do a useful and client friendly web for all users.

Contains all the data obtained of the database that is filled with data from the collaborators to this project. 

Includes all the tools to search specific species or group of species.

<br/>

## <a id="listfiles">Repository Distribution</a>
In this shows a brief explanation of the folder composition of the repository

<br/>

### <a id="folders">_Folders:_</a>
- [API](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/API) -> Where all files that involves the API used to query the database, are stored. 
- [Backend](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Backend) -> Where all files and folders to clean and process the data before submitting it in the database. 
- [bio-cat](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/bio-cat) -> The vue project folder.
database. Not upladed, included in another repository.
- [Mockup](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/tree/main/Mockup) -> Folder where is stored the ideas of the web before creating it.

<br/>

### <a id="files">_Files:_</a>
- [.gitignore](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/.gitignore) -> It's filled with path that are not iteresting to upload in this repository. 
- [docker-compose.yml](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/docker-compose.yml) -> This file configures the running of all the containers and scripts at the same time. 
- [README.md](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/README.md) -> This file.

<br/>

---
## <a id="before-start">Need to know</a>
This project works with a python backend, a conection with a flask api, a mongodb datbase with three collections and a front-end developed in vue.

<br/>

## <a id="liblist">Tools used</a>
- Flask app
- Python
- Vue3
- MongoDB
- Docker
- JSON
- Google API
- Markdown
- Node
  
<br/>

## <a id="steps">Steps and Commands</a>
**There are few steps that requiere .env with local paths for enviroment and passwords.
**Actually the database in the bsc cloud so needs to be connected to vpn to could enter. 

The [docker-compose.yml](https://gitlab.bsc.es/inb/ihcn-biodiversitat/-/blob/main/docker-compose.yml) is the file to execute to see the web in development. Here are the commands to run the project.
<br/>
In the same folder where this file is in the terminal.

To run all the docker containers:

`docker-compose up`

Stop the docker container:

First Ctrl+C to stop the process.

`docker-compose down`

If had been modified any configuration file or database, bqackend file that implies a change in running:

`docker-compose build`

Once modified and done the command above , the run again the first command.

All Frontend modifications are configured in hot reload. There is no need to down the containers and run again.


> To see the web in the explorer must be watched in this path:
>  `http://localhost:8080`

<br/>

## <a id="nowdays">Now working</a>
At this point we are implementing to upload those dockerfile to a virtual machine in bsc cloud to obtain a domain.

With this step that link could be shared and the developers will work with a stable version of the programm before any push.

So now then, we will still upgrading and implementing functionality to the views in the vue framework.