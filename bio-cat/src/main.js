import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import gsap from 'gsap'
import { TextPlugin, RoundPropsPlugin } from 'gsap/all';
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import Antd from 'ant-design-vue';

// Register GSAP plugins
gsap.registerPlugin(TextPlugin, RoundPropsPlugin);
/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';;
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { faHandPointer } from '@fortawesome/free-regular-svg-icons';

// Add the icons to the library
library.add(fab);
library.add(fas);
library.add(faCaretRight);
library.add(faHandPointer);

// Instantiate the Pinia instance
const pinia = createPinia()

const i18n = createI18n({
  legacy: false,
  locale: 'CA', // Set the default language to Spanish
  messages: {
    EN: require('./locales/en.json'),
    ES: require('./locales/es.json'),
    CA: require('./locales/ca.json')
  }
})

createApp(App)
.component('font-awesome-icon', FontAwesomeIcon)
.use(router)
.use(pinia)
.use(gsap)
.use(i18n)
.use(Antd)
.mount('#app')

