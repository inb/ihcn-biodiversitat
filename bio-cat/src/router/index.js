import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/species',
    name: 'species',
    component: () => import('../views/SpeciesView.vue')
  },
  {
    path: '/species/:lookupValue',
    name: 'specieDetails',
    component: () => import('../views/SpecieDetailsView.vue'),
    props: true
  },
  {
    path: '/statistics',
    name: 'statistics',
    component: () => import('../views/StatisticalDataView.vue')
  },
  {
    path: '/news',
    name: 'news',
    component: () => import('../views/NewsView.vue') 
  },
  {
    path: '/legislation',
    name: 'legislation',
    component: () => import('../views/LegislationView.vue') 
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('../views/ContactView.vue') 
  },
  // Wildcard path to redirect any non-matching path to home.
  {
    path: '/:pathMatch(.*)',
    redirect: '/'
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  base: '/biodiversitat/',
  mode: 'history'
  
})

export default router
