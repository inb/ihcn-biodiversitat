# --------------------------------------------------------------------------------------------------------------------
# --- IMPORTS --- #
# --------------------------------------------------------------------------------------------------------------------
from ete3 import NCBITaxa
from flask_cors import CORS
from pymongo import MongoClient
from flask import Flask, json, Response, request
from Bio import Entrez
import requests
import logging
from dotenv import load_dotenv
import os
import re


load_dotenv(
    dotenv_path=os.path.join(os.path.dirname(__file__), "..", ".env")
)  # Load environment variables from .env file


# Set up email (required for inquiries)
Entrez.email = os.getenv("EMAIL")

# --------------------------------------------------------------------------------------------------------------------
# --- API CONTENT --- #
# --------------------------------------------------------------------------------------------------------------------

app = Flask(__name__)
cors = CORS(app)

cors = CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
# Configure the logging
logging.basicConfig(level=logging.DEBUG)  # Set the desired log level


class BioAPI:
    def __init__(self, collection_name):
        try:
            # Read MongoDB connection details from environment variables
            host = os.getenv("MONGODB_HOST")
            database = os.getenv("MONGODB_DATABASE")
            username = os.getenv("MONGODB_USERNAME")
            password = os.getenv("MONGODB_PASSWORD")

            if not (host and database and username and password):
                raise ConnectionError(
                    "Incomplete MongoDB connection details in environment variables."
                )
            self.client = MongoClient(
                f"mongodb://{username}:{password}@{host}/{database}"
            )
            self.database = database
            self.collection_name = collection_name
            self.db = self.client[self.database][self.collection_name]
        except Exception as e:
            raise ConnectionError(
                f"Failed to connect to the MongoDB database: {str(e)}"
            )

    def read(self):
        # Return all documents in the database
        documents = self.db.find()
        output = [
            {item: data[item] for item in data if item != "_id"} for data in documents
        ]
        return output

    def return_species(self):
        documents = self.db.find()
        output = []
        for data in documents:
            species_data = {}  # Define the species_data variable
            species_data = {item: data[item] for item in data if item != "_id"}
            filename = species_data.get("INFORMACIÓ COMPLEMENTÀRIA", {}).get("Pertany_A")
            creationDateFile = self.get_creation_date_of_the_object(filename)
            if creationDateFile:
                species_data["INFORMACIÓ COMPLEMENTÀRIA"]["Data de recol·lecció de les dades"]
            if species_data is not None:
                data_items = [species_data]
                if data_items:
                    output.append(data_items)
        return output


    def return_families(self):
        pipeline = [
            {
                "$group": {
                    "_id": {
                        "classe": "$TAXONOMIA.Classe",
                        "kingdom": "$TAXONOMIA.Regne",
                    },
                    "families": {"$addToSet": "$TAXONOMIA.Família"},
                }
            },
            {
                "$sort": {
                    "_id.kingdom": 1,
                    "_id.classe": 1,
                }
            },
            {
                "$group": {
                    "_id": {"$ifNull": ["$_id.kingdom", "N/D"]},
                    "options": {
                        "$push": {
                            "classe": {"$ifNull": ["$_id.classe", "N/D"]},
                            "families": "$families",
                        }
                    },
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "kingdom": "$_id",
                    "options": 1,
                }
            },
            {
                "$sort": {
                    "kingdom": 1,  # 1 for ascending order, -1 for descending order
                }
            },
        ]

        result = list(self.db.aggregate(pipeline))

        return result

    def return_classes(self):
        pipeline = [
            {
                "$group": {
                    "_id": "$TAXONOMIA.Regne",
                    "classes": {"$addToSet": "$TAXONOMIA.Classe"},
                }
            },
            {"$unwind": "$classes"},
            {"$sort": {"_id": 1, "classes": 1}},  # Sort by Regne and then by classes
            {"$group": {"_id": "$_id", "classes": {"$push": "$classes"}}},
            {
                "$project": {
                    "_id": 0,
                    "label": {
                        "$ifNull": [
                            "$_id",
                            "N/D",
                        ]
                    },
                    "options": "$classes",
                }
            },
            {
                "$sort": {
                    "label": 1,
                }
            },
        ]

        result = list(self.db.aggregate(pipeline))

        return result
    
    def return_divisions(self):
        pipeline = [
            {
                "$group": {
                    "_id": "$TAXONOMIA.Regne",
                    "divisions": {"$addToSet": "$TAXONOMIA.Divisió"},
                }
            },
            {"$unwind": "$divisions"},
            {"$sort": {"_id": 1, "divisions": 1}},  # Sort by Regne and then by classes
            {"$group": {"_id": "$_id", "divisions": {"$push": "$divisions"}}},
            {
                "$project": {
                    "_id": 0,
                    "label": {
                        "$ifNull": [
                            "$_id",
                            "N/D",
                        ]
                    },
                    "options": "$divisions",
                }
            },
            {
                "$sort": {
                    "label": 1,
                }
            },
        ]

        result = list(self.db.aggregate(pipeline))

        return result
    
    def return_order(self):
        pipeline = [
            {
                "$group": {
                    "_id": "$TAXONOMIA.Regne",
                    "orders": {"$addToSet": "$TAXONOMIA.Ordre"},
                }
            },
            {"$unwind": "$orders"},
            {"$sort": {"_id": 1, "orders": 1}},  # Sort by Regne and then by classes
            {"$group": {"_id": "$_id", "orders": {"$push": "$orders"}}},
            {
                "$project": {
                    "_id": 0,
                    "label": {
                        "$ifNull": [
                            "$_id",
                            "N/D",
                        ]
                    },
                    "options": "$orders",
                }
            },
            {
                "$sort": {
                    "label": 1,
                }
            },
        ]

        result = list(self.db.aggregate(pipeline))

        return result

    def return_kingdoms(self):
        # Use distinct to retrieve all unique values for the "Regne" key
        kingdoms = self.db.distinct("TAXONOMIA.Regne")
        # Return the list of unique kingdoms
        return kingdoms

    def count_documents(self):
        # Count the number of documents in the collection
        count = self.db.count_documents({})
        return count

    def count_species_by_kingdom(self):
        kingdom_counts = {}

        entries = self.db.find()
        for entry in entries:
            # Obtener el reino de la entrada actual
            kingdom = entry.get("TAXONOMIA", {}).get("Regne")

            # Si el reino no está definido, se considera como "No definido"
            if kingdom is None:
                kingdom = "N/D"

            kingdom_counts[kingdom] = kingdom_counts.get(kingdom, 0) + 1

        return kingdom_counts

    def count_species_by_uicn(self):
        uicn_counts = {"NULL": 0, "EX": 0, "EW": 0, "CR": 0, "EN": 0, "VU": 0, "NT": 0, "LC": 0, "DD": 0, "NE": 0, "RE": 0, "NA": 0}  

        entries = self.db.find()
        for entry in entries:
            uicn_categories = entry.get("SITUACIÓ", {}).keys()
            uicn_values = [
                entry.get("SITUACIÓ", {}).get(category) for category in uicn_categories
            ]

            # Check if UICN GBIF is present, if so, ignore this entry
            if "UICN GBIF" in uicn_categories:
                continue

            # If no UICN category is present, increment NULL count
            if not any(uicn_values):
                uicn_counts["NULL"] += 1
            else:
                # Contar solo una vez por entrada
                counted = False
                
                situacion = entry.get("SITUACIÓ", {})
                uicn_keys = [key for key in situacion if key.startswith("UICN") and key != "UICN GBIF"]
                if len(uicn_keys) > 0:
                    # Tomar solo una clave UICN si hay más de una
                    uicn_key = uicn_keys[0]
                    categoria = situacion[uicn_key]

                    # Mapear la clave si es necesario
                    key_mapping = {
                        "-": "NULL",
                        ".-": "NULL",
                        "CR (B1a)": "CR",
                        "DD (valoració a l'Àfrica)": "DD",
                        "LC ( consta com F. fragilis)": "LC",
                        "LC (B1a)": "LC",
                        "LC (l'espècie)": "LC",
                        "LC CR (baeticus)": "LC",
                        "N/A": "NA",
                        "No considerada": "NE",
                        "Sense dades": "NE",
                        "VU (B1a)": "VU",
                        "VU?": "VU",
                        "Vulnerable": "VU"
                    }

                    if categoria in key_mapping:
                        categoria = key_mapping[categoria]

                    # Añadir 1 al conteo correspondiente
                    if categoria in uicn_counts:
                        uicn_counts[categoria] += 1
                        counted = True

                if not counted:
                    uicn_counts["NULL"] += 1
                    
        return uicn_counts

    # ---------------------------------------------------------------------------------------------
    # --- FUNCTIONS WITH QUERIES --- #
    # ---------------------------------------------------------------------------------------------

    def get_related_species(self, family) -> list:
        """
        Obtains a list with the ids of the species that belong to the family passed by parameter.
        """

        cursor = self.db.find(
            {"TAXONOMIA.Família": family, "TAXONOMIA.TaxId": {"$exists": True}}
        )

        # Initialize an empty list for taxIDs
        list_taxid_of_species = []

        taxid_count = 0
        max_taxids = 5

        # Iterate over the cursor and get the taxIDs if they are present and not null
        for doc in cursor:
            try:
                taxid = doc["TAXONOMIA"]["TaxId"]
                if taxid is not None:
                    list_taxid_of_species.append(taxid)
                    taxid_count += 1
                    if taxid_count >= max_taxids:
                        break
            except KeyError:
                # Handle KeyError error, e.g. skip document without 'TAXONOMY'
                pass

        return list_taxid_of_species

    def get_data_from_NCBI(self, taxid) -> dict:
        """
        Get data from NCBI Taxonomy for a given species.
        """
        results = {}

        try:

            handle = Entrez.efetch(db="taxonomy", id=taxid, retmode="xml")
            records = Entrez.read(handle)
            handle.close()

            # Get the data of interest
            results["Source"] = "NCBI"
            results["ScientificName"] = records[0].get("ScientificName")
            results["TaxId"] = records[0].get("TaxId")
            results["Rank"] = records[0].get("Rank")
            results["Division"] = records[0].get("Division")

            # Handle the case where 'OtherNames' may not be present
            otherNames = records[0].get("OtherNames", {})
            results["Synonym"] = " ".join(
                otherNames.get("GenbankSynonym", []) + otherNames.get("Synonym", [])
            )

            results["CommonName"] = otherNames.get("GenbankCommonName")
            results["Linaje"] = records[0].get("Lineage")

        except Exception as e:
            # Handle the exception and set the error message in the results
            results["Source"] = "NCBI"
            results["Error"] = "NCBI data not available. Try again later."
            print(f"Error -> Get data from NCBI Taxonomy: {e}")

        return results

    def get_data_from_GBIF(self, specie_name) -> dict:
        """
        Get data from GBIF for a given species
        """
        data_dict = {}

        # Retrieve record for the specie name
        try:
            # Search for occurrences on GBIF API
            search_url = f"https://api.gbif.org/v1/occurrence/search?scientificName={specie_name}"
            response = requests.get(search_url)
            data = response.json()

            # Retrieve gbifID of specie
            if "results" in data and data["results"]:
                specie = data["results"][0]

                # Get the data of interest
                data_dict["Source"] = "GBIF"
                data_dict["GbifId"] = specie.get("gbifID")
                data_dict["Scientific Name"] = specie.get("scientificName")
                data_dict["Kingdom"] = specie.get("kingdom")
                data_dict["Phylum"] = specie.get("phylum")
                data_dict["Order"] = specie.get("order")
                data_dict["Family"] = specie.get("family")
                data_dict["iucn Red List Category"] = specie.get("iucnRedListCategory")
                data_dict["Genus"] = specie.get("genus")
                data_dict["Class"] = specie.get("class")
                data_dict["Collection Code"] = specie.get("collectionCode")

            else:
                print(f"No results were found for the species {specie_name}.")

        except requests.exceptions.RequestException as e:
            data_dict["Source"] = "GBIF"
            data_dict["Error"] = "An error occurred while accessing GBIF API"
            print("An error occurred while accessing GBIF API:", str(e))
        return data_dict

    def get_data_from_Wikipedia(self, specie_name, language) -> dict:
        """
        Get data from Wikipedia for a given species and a given language.
        """
        dataWiki = None

        language = language.lower()
        # Build the Wikipedia API URL
        url = f"https://{language}.wikipedia.org/w/api.php?action=query&list=search&srprop=snippet&format=json&origin=*&utf8=&srsearch={specie_name}"

        try:
            # Make the GET request to the API URL
            response = requests.get(url)
            data = response.json()

            # Extract search results
            results = data["query"]["search"]

            if results:
                # If results were found, return the first result as a snippet.
                first_result = results[0]
                title = first_result["title"]
                snippet = first_result["snippet"]
                pageId = first_result["pageid"]

                if self.validate_snippet(snippet, specie_name):

                    # Create a dictionary with the data you want to return
                    dataWiki = {
                        "Title": title,
                        specie_name + " Snippet": snippet,
                        "pageId": pageId,
                        "Source": "WIKIPEDIA",
                    }
                else:
                    print(f"The snippet for {specie_name} did not pass validation.")
            else:
                print(f"No results were found for the species {specie_name}.")

        except Exception as e:
            # Error handling, for example if the request fails.
            print("An error occurred while accessing WIKIPEDIA API:", str(e))
        return dataWiki

    def validate_snippet(self, snippet, specie_name):
        """
        Validate if the snippet contains specie_name or the words 'especies', 'espècie' or 'especie'.
        """
        keywords = [specie_name, "especies", "espècie", "especie"]
        snippet_lower = snippet.lower()

        # Check if any keyword is present in the snippet
        return any(keyword in snippet_lower for keyword in keywords)

    def get_author_of_the_object(self, filename):
        """
        Returns the author of the species data.
        """

        obj1 = BioAPI("registry")

        documents = obj1.db.find_one(
            {"file_name": filename, "author": {"$exists": True}}
        )
        if documents:
            return documents["author"]
        else:
            return None
        
    def get_creation_date_of_the_object(self, filename):
        """
        Returns the creation date of the species data.
        """

        obj1 = BioAPI("registry")

        documents = obj1.db.find_one(
            {"file_name": filename, "creation_date": {"$exists": True}}
        )
        if documents:
            return documents["creation_date"]
        else:
            return None


    def find_specie(self, specie):
        """Given a TaxID or species name, returns all the data from the database and NCBI API if available"""

        output = []
        if specie.isdigit():
            specie = str(specie)
            regex_pattern = f"{specie}"
            documents = self.db.find(
                {"TAXONOMIA.TaxId": {"$regex": regex_pattern, "$options": "i"}}
            )
            for data in documents:
                species_data = {item: data[item] for item in data if item != "_id"}
                filename = species_data.get("INFORMACIÓ COMPLEMENTÀRIA", {}).get(
                    "Pertany_A"
                )

                # Delete UICN GBIF
                if (
                    "SITUACIÓ" in species_data
                    and "UICN GBIF" in species_data["SITUACIÓ"]
                ):
                    del species_data["SITUACIÓ"]["UICN GBIF"]

                # Add author
                author = self.get_author_of_the_object(filename)
                if author:
                    species_data["INFORMACIÓ COMPLEMENTÀRIA"]["Contribuïdors"] = author

                #Add creation_date 
                creationDateFile = self.get_creation_date_of_the_object(filename)
                if creationDateFile:
                    species_data["INFORMACIÓ COMPLEMENTÀRIA"]["Data de recol·lecció de les dades"] = creationDateFile

                # NCBI Data
                ncbi_data = self.get_data_from_NCBI(species_data["TAXONOMIA"]["TaxId"])

                # GBIF Data
                gbif_data = self.get_data_from_GBIF(
                    species_data["TAXONOMIA"]["Espècie"]
                )

                if species_data is not None:
                    data_items = [species_data]

                    if ncbi_data is not None:
                        data_items.append(ncbi_data)
                    if gbif_data is not None:
                        data_items.append(gbif_data)

                    if data_items:
                        output.append(data_items)

        else:
            # Escape special characters in the species string and make species search more flexible
            specie_words = specie.split()
            regex_pattern = ".*".join(map(re.escape, specie_words))
            documents = self.db.find(
                {"TAXONOMIA.Espècie": {"$regex": regex_pattern, "$options": "i"}}
            )
            # If there are results for the species name search, get the TaxID
            for data in documents:
                species_data = {item: data[item] for item in data if item != "_id"}

                # Delete UICN GBIF
                if (
                    "SITUACIÓ" in species_data
                    and "UICN GBIF" in species_data["SITUACIÓ"]
                ):
                    del species_data["SITUACIÓ"]["UICN GBIF"]

                # Add author
                filename = species_data.get("INFORMACIÓ COMPLEMENTÀRIA", {}).get(
                    "Pertany_A"
                )
                author = self.get_author_of_the_object(filename)
                if author:
                    species_data["INFORMACIÓ COMPLEMENTÀRIA"]["Contribuïdors"] = author

                #Add creation_date 
                creationDateFile = self.get_creation_date_of_the_object(filename)
                if creationDateFile:
                    species_data["INFORMACIÓ COMPLEMENTÀRIA"]["Data de recol·lecció de les dades"] = creationDateFile

                # GET DATA NCBI
                taxid = species_data.get("TAXONOMIA", {}).get("TaxId")
                if taxid is not None:
                    ncbi_data = self.get_data_from_NCBI(taxid)
                else:
                    ncbi_data = None

                # ----------------------------------------------------------------

                gbif_data = self.get_data_from_GBIF(
                    species_data["TAXONOMIA"]["Espècie"]
                )

                if species_data is not None:
                    data_items = [species_data]

                    if ncbi_data is not None:
                        data_items.append(ncbi_data)
                    if gbif_data is not None:
                        data_items.append(gbif_data)

                    if data_items:
                        output.append(data_items)

        return output

    def find_classes(self, kingdoms: list[str]):
        kingdom_list = kingdoms.split(",")

        # Create a list to store the results
        results = []

        # Iterate over the specified kingdoms
        for kingdom in kingdom_list:
            if kingdom == "N/D":
                # Handle the "N/D" case by replacing it with None
                kingdom_value = None
            else:
                kingdom_value = kingdom

            app.logger.info(f"Querying for kingdom: {kingdom_value}")

            # Construct a query based on the kingdom value
            query = {"TAXONOMIA.Regne": kingdom_value}

            # Find documents based on the query
            documents = self.db.find(query)

            # Extract the unique classes for this kingdom
            unique_classes = set()
            for document in documents:
                unique_classes.add(document["TAXONOMIA"]["Classe"])

            # Convert the set of unique classes to a sorted list
            classes = sorted(unique_classes, key=lambda x: (x is not None, x))

            # Create a dictionary for this kingdom and its classes
            result_dict = {"label": kingdom_value, "options": classes}

            # Add the dictionary to the results list
            results.append(result_dict)

        return results

    def find_families(self, kingdoms=None, classes=None):
        kingdoms_list = []
        classes_list = []

        if kingdoms:
            kingdoms_list = kingdoms.split(",")

        if classes:
            classes_list = classes.split(",")

        # Create a set to store unique families

        results = []

        # If both kingdoms and classes are provided
        if kingdoms_list and classes_list:
            kingdom_results = {}
            for kingdom in kingdoms_list:
                # Handle "N/D" case
                kingdom_value = None if kingdom == "N/D" else kingdom

                # Initialize the dictionary for the current kingdom
                kingdom_dict = {"kingdom": kingdom_value, "options": []}

                for class_name in classes_list:
                    class_value = None if class_name == "N/D" else class_name
                    app.logger.info(
                        f"Querying for kingdom: {kingdom_value}, class: {class_value}"
                    )

                    # Construct a query based on the kingdom and class values
                    query = {
                        "TAXONOMIA.Regne": kingdom_value,
                        "TAXONOMIA.Classe": class_value,
                    }

                    # Find documents based on the query
                    documents = self.db.find(query)

                    unique_families = set()

                    # Iterate over the documents and extract the unique families
                    for document in documents:
                        unique_families.add(document["TAXONOMIA"]["Família"])

                    # Convert the set of unique families to a list and return it
                    families = sorted(unique_families, key=lambda x: (x is not None, x))

                    # Check if families exist before adding the option
                    if families:
                        # Create a dictionary for this class and its families
                        result_dict = {"classe": class_value, "families": families}

                        # Add the dictionary to the options list for the current kingdom
                        kingdom_dict["options"].append(result_dict)

                # Check if options exist before adding the kingdom to the results
                if kingdom_dict["options"]:
                    # Add the kingdom dictionary to the kingdom_results dictionary
                    kingdom_results[kingdom_value] = kingdom_dict

            # Convert the values of the kingdom_results dictionary to a list and return it
            results.extend(list(kingdom_results.values()))

        elif kingdoms_list:
            kingdom_results = {}

            for kingdom in kingdoms_list:
                # Handle "N/D" case
                kingdom_value = None if kingdom == "N/D" else kingdom

                # Construct a query based on the kingdom value
                query_kingdom = {"TAXONOMIA.Regne": kingdom_value}
                # Find documents based on the query
                documents_kingdom = self.db.find(query_kingdom)

                # Create a list to store unique classes for this kingdom
                unique_classes = set()

                # Iterate over the documents and extract the unique classes
                for document in documents_kingdom:
                    class_value = document["TAXONOMIA"]["Classe"]
                    unique_classes.add(class_value)

                # Iterate over the unique classes and create dictionaries for each
                for class_value in unique_classes:
                    # Construct a query based on the kingdom and class values
                    query = {
                        "TAXONOMIA.Regne": kingdom_value,
                        "TAXONOMIA.Classe": class_value,
                    }

                    # Find documents based on the query
                    documents = self.db.find(query)

                    unique_families = set()

                    # Iterate over the documents and extract the unique families
                    for document in documents:
                        unique_families.add(document["TAXONOMIA"]["Família"])

                    # Convert the set of unique families to a list and return it
                    families = sorted(unique_families, key=lambda x: (x is not None, x))
                    # Create a dictionary for this kingdom and class combination and its families
                    result_dict = {"classe": class_value, "families": families}

                    # Add the dictionary to the kingdom_results dictionary
                    if kingdom_value not in kingdom_results:
                        kingdom_results[kingdom_value] = {
                            "kingdom": kingdom_value,
                            "options": [result_dict],
                        }
                    else:
                        kingdom_results[kingdom_value]["options"].append(result_dict)

            # Convert the values of the kingdom_results dictionary to a list and return it
            results.extend(list(kingdom_results.values()))

        # If only classes are provided
        elif classes_list:
            kingdom_results = {}
            for class_name in classes_list:
                class_value = None if class_name == "N/D" else class_name

                # Construct a query based on the class value
                query_classe = {"TAXONOMIA.Classe": class_value}
                # Find documents based on the query
                documents_classe = self.db.find(query_classe)

                # Create a list to store unique classes for this kingdom
                unique_kingdoms = set()

                # Iterate over the documents and extract the unique classes
                for document in documents_classe:
                    kingdom_value = document["TAXONOMIA"]["Regne"]
                    unique_kingdoms.add(kingdom_value)

                # Iterate over the unique classes and create dictionaries for each
                for king_value in unique_kingdoms:
                    # Construct a query based on the kingdom and class values
                    query = {
                        "TAXONOMIA.Regne": king_value,
                        "TAXONOMIA.Classe": class_value,
                    }

                    # Find documents based on the query
                    documents = self.db.find(query)

                    unique_families = set()

                    # Iterate over the documents and extract the unique families
                    for document in documents:
                        unique_families.add(document["TAXONOMIA"]["Família"])

                    # Convert the set of unique families to a list and return it
                    families = sorted(unique_families, key=lambda x: (x is not None, x))
                    # Create a dictionary for this kingdom and class combination and its families
                    result_dict = {"classe": class_value, "families": families}

                    # Add the dictionary to the kingdom_results dictionary
                    if kingdom_value not in kingdom_results:
                        kingdom_results[kingdom_value] = {
                            "kingdom": kingdom_value,
                            "options": [result_dict],
                        }
                    else:
                        kingdom_results[kingdom_value]["options"].append(result_dict)

            # Convert the values of the kingdom_results dictionary to a list and return it
            results.extend(list(kingdom_results.values()))

        return results

    def find_divisions(self, divisions: list[str]):
        find_divisions = divisions.split(",")

        # Create a list to store the results
        results = []

        # Iterate over the specified kingdoms
        for division in find_divisions:
            if division == "N/D":
                # Handle the "N/D" case by replacing it with None
                division_value = None
            else:
                division_value = division

            app.logger.info(f"Querying for kingdom: {division_value}")

            # Construct a query based on the kingdom value
            query = {"TAXONOMIA.Divisió": division_value}

            # Find documents based on the query
            documents = self.db.find(query)

            # Extract the unique classes for this kingdom
            unique_classes = set()
            for document in documents:
                unique_classes.add(document["TAXONOMIA"]["Divisió"])

            # Convert the set of unique classes to a sorted list
            classes = sorted(unique_classes, key=lambda x: (x is not None, x))

            # Create a dictionary for this kingdom and its classes
            result_dict = {"label": division_value, "options": classes}

            # Add the dictionary to the results list
            results.append(result_dict)

        return results

    def find_order(self, orders: list[str]):
        find_orders = orders.split(",")

        # Create a list to store the results
        results = []

        # Iterate over the specified kingdoms
        for order in find_orders:
            if order == "N/D":
                # Handle the "N/D" case by replacing it with None
                order_value = None
            else:
                order_value = order

            app.logger.info(f"Querying for kingdom: {order_value}")

            # Construct a query based on the kingdom value
            query = {"TAXONOMIA.Ordre": order_value}

            # Find documents based on the query
            documents = self.db.find(query)

            # Extract the unique classes for this kingdom
            unique_classes = set()
            for document in documents:
                unique_classes.add(document["TAXONOMIA"]["Ordre"])

            # Convert the set of unique classes to a sorted list
            classes = sorted(unique_classes, key=lambda x: (x is not None, x))

            # Create a dictionary for this kingdom and its classes
            result_dict = {"label": order_value, "options": classes}

            # Add the dictionary to the results list
            results.append(result_dict)

        return results

    def get_species_suggestions(self, query):
        """Returns species suggestions for a given query, Taxid or specie name"""

        if query.isdigit():
            regex_pattern = f"^{query}"
            documents = self.db.find(
                {"TAXONOMIA.TaxId": {"$regex": regex_pattern, "$options": "i"}}
            )
        else:
            regex_pattern = f"^{query}"
            documents = self.db.find({"$or": [
                {"NOM DE L'AUTOR.Autor": {"$regex": regex_pattern, "$options": "i" }} ,
                {"TAXONOMIA.Espècie": {"$regex": regex_pattern, "$options": "i"}}
                ]
            })

        suggestions = [
            {
                **data["TAXONOMIA"],
                **data["INFORMACIÓ COMPLEMENTÀRIA"],
                **data["SITUACIÓ"],
                **data["NOM DE L'AUTOR"]
            }
            for data in documents
        ]

        return suggestions

    def get_taxid_by_class(self, classe, families=None):
        """Returns a list of TaxIds filtered by Class and optionally by family"""
        families_list = []
        # Create a set to store unique taxids
        unique_taxid = set()

        if classe is not None:
            if families:
                families_list = families.split(",")
                # Replace "N/D" with None in the families_list
                families_list = [
                    family if family != "N/D" else None for family in families_list
                ]

                # Create a list of queries for each family
                family_queries = [
                    {"TAXONOMIA.Família": family} for family in families_list
                ]

                # Construct a query based on the Class and a list of family queries
                query = {"TAXONOMIA.Classe": classe, "$or": family_queries}

            else:
                query = {"TAXONOMIA.Classe": classe}

            # Find documents based on the query
            documents = self.db.find(query)

            # Iterate over the documents and extract the unique taxIDs
            for document in documents:
                unique_taxid.add(document["TAXONOMIA"]["TaxId"])

            # Convert the set of unique taxIDs to a list, filter out None values, and return it
            list_taxId = sorted([taxid for taxid in unique_taxid if taxid is not None])
            return list_taxId

    def get_phylotree(self, classe, families=None):
        """Retrieves a phylogenetic tree in Newick format for a specified taxonomic class and optional families."""

        ncbi = NCBITaxa()

        listTaxIds = self.get_taxid_by_class(classe, families)

        # Obtain the taxonomic tree of the species in Newick format with complete hierarchy
        tree = ncbi.get_topology(listTaxIds, intermediate_nodes=True)

        # Asignar nombre a las ramas donde hay bifurcaciones
        for node in tree.traverse():
            if node.is_leaf() or len(node.get_children()) > 1:
                node.name = node.sci_name if hasattr(node, "sci_name") else ""
            else:
                # De lo contrario, dejamos el nombre en blanco
                node.name = ""

        # Asignar nombres específicos para los nodos con rango 'kingdom' y 'family'
        for node in tree.traverse():
            if node.rank == "kingdom" or node.rank == "class" or node.rank == "species":
                node.name = node.sci_name if hasattr(node, "sci_name") else ""

        # Get the tree in Newick format (format 1) with the species names on the labels
        tree_data = tree.write(format=1)

        if len(listTaxIds) == 1:
            tree_data = f"({tree_data})"
            return tree_data
        else:
            return tree_data

    def get_allFamilies(self, classe):
        """Returns list of families with at least one species having a non-null TaxId"""

        unique_families = set()

        class_value = None if classe == "N/D" else classe
        # app.logger.info(f"Querying for class: {class_value}")

        # Construct a query based on the class value
        query = {"TAXONOMIA.Classe": class_value}

        # Find documents based on the query
        documents = self.db.find(query)

        # Iterate over the documents and extract the unique families with at least one non-null TaxId
        for document in documents:
            if document["TAXONOMIA"]["TaxId"] != None:
                unique_families.add(document["TAXONOMIA"]["Família"])

        # Convert the set of unique families to a list and return it
        families = sorted(unique_families, key=lambda x: (x is not None, x))
        return families


# --------------------------------------------------------------------------------------------------------------------
# --- APP ROUNTING --- #
# --------------------------------------------------------------------------------------------------------------------


@app.route("/")
def base():
    """Control route to check if the api works"""
    return Response(
        response=json.dumps({"Status": "UP"}), status=200, mimetype="application/json"
    )


@app.route("/Biodb/all", methods=["GET"])
def mongo_read():
    """Returns all the documents in the database"""
    obj1 = BioAPI("species")
    response = obj1.read()
    return Response(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


@app.route("/Biodb/count", methods=["GET"])
def mongo_count():
    """Returns the count of all the documents in the collection"""
    obj1 = BioAPI("species")
    count = obj1.count_documents()
    response = {"count": count}
    return Response(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


@app.route("/Biodb/kingdoms", methods=["GET"])
def mongo_all_kingdoms():
    """Returns all the unique kingdoms in the collection"""
    obj1 = BioAPI("species")
    kingdoms: list[str] = obj1.return_kingdoms()
    return Response(
        response=json.dumps(kingdoms), status=200, mimetype="application/json"
    )


@app.route("/Biodb/specie/<specie>", methods=["GET"])
def mongo_species(specie):
    """Returs the document or documents that contain the given query term, includes a misspelling save"""
    obj1 = BioAPI("species")
    response = obj1.find_specie(specie)
    if len(response) == 0:
        return Response(
            response=json.dumps({"error": "Specie not found"}),
            status=404,
            mimetype="application/json",
        )
    return Response(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


@app.route("/Biodb/classes", methods=["GET"])
def mongo_classes():
    """Returns the ordered list of unique classes for the given kingdoms or all unique classes if no kingdoms are specified"""
    kingdoms_list = request.args.get("kingdoms")
    # app.logger.info(kingdoms_list)
    obj1 = BioAPI("species")

    if kingdoms_list:
        # If kingdoms are specified, return classes for those kingdoms
        classes = obj1.find_classes(kingdoms_list)
    else:
        # If no kingdoms are specified, return all unique classes
        classes = obj1.return_classes()

    return Response(
        response=json.dumps(classes), status=200, mimetype="application/json"
    )


@app.route("/Biodb/families", methods=["GET"])
def mongo_families():
    """Returns unique families based on provided parameters"""
    kingdoms = request.args.get("kingdoms")
    classes = request.args.get("classes")

    obj1 = BioAPI("species")

    if kingdoms or classes:
        # If either kingdoms or classes are specified, call find_families with the provided parameters
        families = obj1.find_families(kingdoms=kingdoms, classes=classes)
    else:
        # If no kingdoms or classes are specified, return all unique families
        families = obj1.return_families()

    return Response(
        response=json.dumps(families), status=200, mimetype="application/json"
    )

@app.route("/Biodb/divisions", methods=["GET"])
def mongo_division():
    """Returns the ordered list of unique classes for the given kingdoms or all unique classes if no kingdoms are specified"""
    divisions_list = request.args.get("divisions")
    # app.logger.info(kingdoms_list)
    obj1 = BioAPI("species")

    if divisions_list:
        # If kingdoms are specified, return classes for those kingdoms
        divisions = obj1.find_divisions(divisions_list)
    else:
        # If no kingdoms are specified, return all unique classes
        divisions = obj1.return_divisions()

    return Response(
        response=json.dumps(divisions), status=200, mimetype="application/json"
    )

@app.route("/Biodb/order", methods=["GET"])
def mongo_order():
    """Returns the ordered list of unique order for the given kingdoms or all unique classes if no kingdoms are specified"""
    order_list = request.args.get("order")
    # app.logger.info(kingdoms_list)
    obj1 = BioAPI("species")

    if order_list:
        # If kingdoms are specified, return classes for those kingdoms
        order = obj1.find_order(order_list)
    else:
        # If no kingdoms are specified, return all unique classes
        order = obj1.return_order()

    return Response(
        response=json.dumps(order), status=200, mimetype="application/json"
    )

@app.route("/Biodb/suggestions/<query>", methods=["GET"])
def mongo_suggestions(query):
    """Returns a list of species suggestions based on the provided query"""
    obj1 = BioAPI("species")

    suggestions = obj1.get_species_suggestions(query)

    return Response(
        response=json.dumps(suggestions), status=200, mimetype="application/json"
    )


@app.route("/Biodb/phylotree", methods=["GET"])
def phylotree():
    """Returns unique taxid based on provided parameters"""
    classe = request.args.get("classe")
    families = request.args.get("families")

    obj1 = BioAPI("species")

    phylotree = obj1.get_phylotree(classe, families)

    return Response(
        response=json.dumps(phylotree), status=200, mimetype="application/json"
    )


@app.route("/Biodb/WikipediaData/<specieName>/<language>", methods=["GET"])
def fetch_wikipedia_data(specieName, language):
    """Returns Wikipedia data"""
    obj1 = BioAPI("species")

    wikipediaData = obj1.get_data_from_Wikipedia(specieName, language)

    return Response(
        response=json.dumps(wikipediaData), status=200, mimetype="application/json"
    )


@app.route("/Biodb/allFamilies", methods=["GET"])
def fetch_allFamilies():
    """Returns all families"""
    classe = request.args.get("classe")
    obj1 = BioAPI("species")

    allFamilies = obj1.get_allFamilies(classe)

    return Response(
        response=json.dumps(allFamilies), status=200, mimetype="application/json"
    )


# Statistical Data
@app.route("/Biodb/statisticalData", methods=["GET"])
def get_statistical_data():
    """Returns the count of all the documents in the collection"""
    obj1 = BioAPI("species")
    countByKingdom = obj1.count_species_by_kingdom()
    # countByUICN = obj1.count_species_by_uicn()
    countByUICN = {"UICN": "In process"}
    response = {"statisticalData": [countByKingdom, countByUICN]}
    return Response(
        response=json.dumps(response), status=200, mimetype="application/json"
    )


# --------------------------------------------------------------------------------------------------------------------
# --- MAIN --- #
# --------------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    app.run(debug=True, port=5001, host="0.0.0.0")
