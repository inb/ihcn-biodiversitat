#--------------------------------------------------------------------------------------------------------------------
# --- IMPORTS --- #
#--------------------------------------------------------------------------------------------------------------------

import os
import pickle
import smtplib
import datetime
import configparser
from pathlib import Path
from dotenv import load_dotenv
from email.message import EmailMessage
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow

#--------------------------------------------------------------------------------------------------------------------
# --- CONSTANTS --- #
#--------------------------------------------------------------------------------------------------------------------
# Get the path to the directory containing the script
script_directory = Path(__file__).parent

# Get the path to the parent directory (assuming the config.ini file is at the same level as API_Google)
# parent_directory = os.path.dirname(script_directory)

# Get the path to the config.ini file
config_file_path = script_directory.parent / "Configuration" / "config.ini"
config = configparser.ConfigParser()
config.read(config_file_path)

WORKED_DIR = Path(config.get('ROUTES', 'worked_dir', fallback='Backend/Files/excels/')).resolve()
Folder_id   = "1Izk9j-DyULAdNi0WbY8DWliUH2h5M8Dk"
SCOPES = ['https://www.googleapis.com/auth/drive']

RECIVERS_EMAIL_ADDRESS  = config.get('MAIL', 'reciver_email_address').split(' ')
SENDER_EMAIL_ADDRESS    = config.get('MAIL', 'sender_email_address')

# Load environment variables from .env file
env_path = Path(__file__).resolve().parent.parent.parent / ".env"

load_dotenv(dotenv_path=env_path)

# Construct the absolute path for the client secret file
client_secret_path = script_directory / 'google_api_config' / 'client_secret.json'

#--------------------------------------------------------------------------------------------------------------------
# --- CONNECTION GOOGLE DRIVE --- #
#--------------------------------------------------------------------------------------------------------------------


def get_drive_service():
    creds = None
    token_path = script_directory / 'google_api_config' / 'token.pickle'

    # Load credentials if available
    if token_path.exists():
        with open(token_path, 'rb') as token:
            creds = pickle.load(token)

    # Handle expired or missing credentials
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secret_path, SCOPES)
            creds = flow.run_local_server(port=9090)

        # Save the credentials for reuse
        with open(token_path, 'wb') as token:
            pickle.dump(creds, token)

    return build('drive', 'v3', credentials=creds)


def compare_directories(local_path, drive_folder_id):

    service = get_drive_service()

    # Retrieve list of files in Google Drive folder
    results = service.files().list(
        q=f"'{drive_folder_id}' in parents",
        fields="nextPageToken, files(id, name, size, modifiedTime)", pageSize=1000).execute()
    drive_files = results.get('files', [])

    # Get list of local files
    local_files = [file.name for file in local_path.iterdir() if file.is_file()]

    new_files = []
    modified_files = []

    print(f"Local path: {local_path}")


    for file in drive_files:
        local_file_path = local_path / file['name']
        if not local_file_path.exists():
            new_files.append(file["name"])
   
        else:
            local_file_mtime = local_file_path.stat().st_mtime
            drive_file_mtime = datetime.datetime.strptime(
                file['modifiedTime'], '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
            if local_file_mtime < drive_file_mtime:
                modified_files.append(file["name"])

    # Check for deleted files in Google Drive directory
    deleted_files = [file for file in local_files if not any(drive_file['name'] == file for drive_file in drive_files)]

    return modified_files, deleted_files, new_files
    
#--------------------------------------------------------------------------------------------------------------------
# --- ADMIN RESULT GOOGLE DRIVE --- #
#--------------------------------------------------------------------------------------------------------------------

def results_compare(result: tuple[list, list, list]) -> str:

    """
    Examinate the result of compare_files and give ti user a response
    Param: result (list)
    Returns: response (str)
    """

    if result[0] == [] and result[1] == [] and result[2] == []:
        response =("No new files or modified files found")

    elif result[0] == [] and result[1] == [] and result[2]!= []:
        response =(f'These are the new files need to be added: {result[2]}\n\nIn total, you need to add: {len(result[2])} new files.')

    elif result[0]== [] and result[1] != [] and result[2]== []:
        response =(f'These files had been deleted: {result[1]}\n\nIn total, you need to delete: {len(result[1])} old files.')

    elif result[0]!= [] and result[1]== [] and result[2] == []:
        response =(f'These are the new files need to be modified: {result[0]}\n\nIn total, you need to modify: {len(result[0])} files.')

    elif result[0]!= [] and result[1]!= [] and result[2] == []:
        response =(f'These files have been deleted: {result[1]}, \nand these are the new files need to be modified: {result[0]}\n\nIn total, you need to delete: {len(result[1])} old files. And modify: {len(result[0])} files.')

    elif result[0] == [] and result[1]!= [] and result[2]!= []:
        response =(f'These files have been deleted: {result[1]}, \nand these are the new files need to be added: {result[0]}\n\nIn total, you need to delete: {len(result[1])} old files. And add: {len(result[0])} new files.')

    elif result[0] != [] and result[1] == [] and result[2]!= []:
        response =(f'These files have been added: {result[2]}, \nand these are the new files need to be modified: {result[0]}\n\nIn total, you need to add: {len(result[2])} new files. And modify: {len(result[0])} files.')
    else:
        response =(f'These are the files that need to be updated: {result[0]}, the new ones to work: {result[2]}, \nand the files that have been erased from the google drive directory: {result[1]}\n\nIn total, you need to delete: {len(result[1])} oldfiles.\nModify: {len(result[0])} files.\nAnd add: {len(result[2])} new files.')

    return response

#--------------------------------------------------------------------------------------------------------------------
# --- SEND MAIL WITH RESULT --- #
#--------------------------------------------------------------------------------------------------------------------

def send_mail(subject, response_content, sender_email, sender_pass, receiver_email, email_smtp, email_port):

    # Create mail object
    message = EmailMessage()

    email_subject  = subject   # Email subject
    sender_email   = sender_email  # Email sender email address
    receiver_email = receiver_email   # Email receiver email address
    response_str   = response_content # Email message content
    email_password = sender_pass       # Email sender password
    email_smtp     = email_smtp    # Email server smtp address
    
    # Configure email headers 
    message['Subject'] = email_subject 
    message['From']    = sender_email 
    message['To']      = receiver_email
    message.set_content(response_str)

    # Conect with server SMTP
    with smtplib.SMTP_SSL(email_smtp, email_port) as smtp:
        try:
            smtp.login(sender_email, email_password) # Login 
            smtp.send_message(message)               # Send message
        except smtplib.SMTPAuthenticationError:
            print(f"Failed to send email to {receiver_email}: Invalid email or password.")
        except Exception as e:
            print(f"Failed to send email to {receiver_email}: {e}")

#--------------------------------------------------------------------------------------------------------------------
# --- MAIN --- #
#--------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    # Check if the client secret path is valid
    if not client_secret_path.exists():
        print(f"Client secret file not found at: {client_secret_path}")
        exit(1)
    
    # Check the differences between directories
    files_diff: tuple[list, list, list] = compare_directories(WORKED_DIR, Folder_id)
    response_str: str = results_compare(files_diff)

    # Send the response via mail
    for reciver_email in RECIVERS_EMAIL_ADDRESS:
        send_mail(
            subject="[IHCN-Biodiversitat] Weekly checking drive changes",
            response_content=response_str + f" \nToday's date {datetime.date.today()} ",
            sender_email=SENDER_EMAIL_ADDRESS,
            sender_pass=os.getenv('EMAIL_PASSWORD'),
            receiver_email=reciver_email,
            email_smtp='mail.bsc.es',
            email_port=465,
        )
